# 页签 (Tabs)
```tsx

import { Tabs , Tab, TabContainer} from '@casstime/bricks';

<Tabs
  activeKey={this.state.activeKey}
  onChange={(value) => this.onChange(value)}
>
  <Tab value="a" >标签一</Tab>
  <Tab value="b" >标签二</Tab>
  <Tab value="c" >标签三</Tab>
</Tabs>
<TabContainer>
  {this.state.activeKey === 'a' && <div> 我是标签一的内容</div>}
  {this.state.activeKey === 'b' && <div> 我是标签二的内容</div>}
  {this.state.activeKey === 'c' && <div> 我是标签三的内容</div>}
</TabContainer>

```

### 一般使用
:::demo

```tsx

interface ITabsProps {
  activeKey?: string;
  onChange: (activeKey: string) => void;
  className?: string;
  style?: React.CSSProperties;
}

interface ITabsState {
  activeKey?: string;
}

class Demo extends React.Component<ITabsProps, ITabsState> {
  constructor(props: any) {
    super(props);
    this.state = {
      activeKey: 'a',
    };

    this.onChange = this.onChange.bind(this);
  }
  public onChange(activeKeyIndex: string) {
    this.setState(() => {
      return {
        activeKey: activeKeyIndex,
      };
    });
  }

  public render() {
    return (
      <div>
        <Tabs
          activeKey={this.state.activeKey}
          onChange={(value) => this.onChange(value)}
        >
          <Tab value="a" >标签一</Tab>
          <Tab value="b" disabled>标签二</Tab>
          <Tab value="c" >标签三</Tab>
        </Tabs>
        <TabContainer>
          {this.state.activeKey === 'a' && <div> 我是标签一的内容</div>}
          {this.state.activeKey === 'b' && <div> 我是标签二的内容</div>}
          {this.state.activeKey === 'c' && <div> 我是标签三的内容</div>}
        </TabContainer>
      </div >
    );
  }
}

```
:::

### 标签的位置可控
:::demo

```tsx

interface ITabsProps {
  activeKey?: string;
  onChange: (activeKey: string) => void;
  className?: string;
  style?: React.CSSProperties;
}

interface ITabsState {
  activeKey?: string;
}

class Demo extends React.Component<ITabsProps, ITabsState> {
  constructor(props: any) {
    super(props);
    this.state = {
      activeKey: 'a',
    };
    this.onChange = this.onChange.bind(this);
  }

  public onChange(activeKeyIndex: string) {
    this.setState(() => {
      return {
        activeKey: activeKeyIndex,
      };
    });
  }

  public render() {
    return (
      <div>
        <Tabs
          activeKey={this.state.activeKey}
          onChange={(value) => this.onChange(value)}
          align="center"
        >
          <Tab value="a" >标签一</Tab>
          <Tab value="b" >标签二</Tab>
          <Tab value="c" >标签三</Tab>
        </Tabs>
        <TabContainer>
          {this.state.activeKey === 'a' && <div> 我是标签一的内容</div>}
          {this.state.activeKey === 'b' && <div> 我是标签二的内容</div>}
          {this.state.activeKey === 'c' && <div> 我是标签三的内容</div>}
        </TabContainer>
      </div >
    );
  }
}

```
:::

### 标签可封装
:::demo

```tsx

interface ITabsProps {
  activeKey?: string;
  onChange: (activeKey: string) => void;
  className?: string;
  style?: React.CSSProperties;
}

interface ITabsState {
  activeKey?: string;
}

class Demo extends React.Component<ITabsProps, ITabsState> {
  constructor(props: any) {
    super(props);
    this.state = {
      activeKey: 'a',
    };
    this.onChange = this.onChange.bind(this);
  }

  public onChange(activeKeyIndex: string) {
    this.setState(() => {
      return {
        activeKey: activeKeyIndex,
      };
    });
  }

  public render() {
    return (
      <div>
        <Tabs
          activeKey={this.state.activeKey}
          onChange={(value) => this.onChange(value)}
          align="right"
        >
          <Tab value="a" ><Button outline>标签一</Button></Tab>
          <Tab value="b" ><Button outline>标签二</Button></Tab>
        </Tabs>
        <TabContainer>
          {this.state.activeKey === 'a' && <div> 我是标签一的内容</div>}
          {this.state.activeKey === 'b' && <div> 我是标签二的内容</div>}
        </TabContainer>
      </div >
    );
  }
}


```
:::

### 左右两边同时使用
:::demo

```tsx

interface ITabsProps {
  activeKey?: string;
  onChange: (activeKey: string) => void;
  className?: string;
  style?: React.CSSProperties;
}

interface ITabsState {
  activeKey?: string;
}

class Demo extends React.Component<ITabsProps, ITabsState> {
  constructor(props: any) {
    super(props);
    this.state = {
      activeKey: 'a',
    };
    this.onChange = this.onChange.bind(this);
  }

  public onChange(activeKeyIndex: string) {
    this.setState(() => {
      return {
        activeKey: activeKeyIndex,
      };
    });
  }

  public render() {
    return (
      <div>
        <Row>
          <Col sm={6} md={6} lg={6}>
            <Tabs
              activeKey={this.state.activeKey}
              onChange={(value) => this.onChange(value)}
            >
              <Tab value="a" >标签一</Tab>
              <Tab value="b" >标签二</Tab>
              <Tab value="c" >标签三</Tab>
            </Tabs>
          </Col>
          <Col sm={6} md={6} lg={6}>
            <Tabs
              activeKey={this.state.activeKey}
              onChange={(value) => this.onChange(value)}
              align='right'
            >
              <Tab value="d" >标签四</Tab>
              <Tab value="e" >标签五</Tab>
            </Tabs>
          </Col>
        </Row>
        <TabContainer>
          {this.state.activeKey === 'a' && <div> 我是左侧标签一的内容</div>}
          {this.state.activeKey === 'b' && <div> 我是左侧标签二的内容</div>}
          {this.state.activeKey === 'c' && <div> 我是左侧标签三的内容</div>}
          {this.state.activeKey === 'd' && <div> 我是右侧标签四的内容</div>}
          {this.state.activeKey === 'e' && <div> 我是右侧标签五的内容</div>}
        </TabContainer>
      </div >
    );
  }
}

```
:::



## Tabs组件 Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| activeKey | string | null | 当前激活状态下的标签 |
| onChange | function:(activeKey: string) => void | -- | 点击事件 |
| children | Array<React.ReactElement<ITabItemProps>> | -- | 包含的子标签 |
| align | string('center', 'right', 'left') | -- | 控制标签的位置居中，靠右，靠左 |
| className | string | undefined | 自定义className |
| style | React.CSSProperties | undefined | 自定义style |

## Tab子组件 Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| children | React.ReactNode | null | 标题（可以封装文字或者图标） |
| value | string | '' | 标签唯一标识 |
| active | boolean | false | 是否是激活状态 |
| disabled | boolean | false | 禁用状态 |
| onChange | function:(value: string) => void | -- | 点击事件 |
| className | string | undefined | 自定义className |
| style | React.CSSProperties | undefined | 自定义style |

## TabContainer子组件  描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| className | string | undefined | 自定义className |
| style | React.CSSProperties | undefined | 自定义style |
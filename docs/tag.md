# Tag
```tsx
import { Tag } from '@casstime/bricks';

<Tag title="我是title" onClick={this.handleClick} onClose={this.handleClose}>Tag1</Tag>
```
### 一般使用
:::demo Tag组件示例

```tsx
class Demo extends React.Component<{}, {}> {
  handleClick() {
    alert('tag click');
  }
  handleClose() {
    alert('tag close')
  }
  render() {
    return <Tag title="我是title" onClick={this.handleClick.bind(this)} onClose={this.handleClose.bind(this)}><a href="javascript:;">Tag1</a></Tag>
  }
}

```
:::

## Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| className | string | undefined | 自定义className |
| style | React.CSSProperties | undefined | 自定义style |
| title | string | '' | 可选，同原生title |
| onClick | Function() | null | 点击回调函数 |
| onClose | Function() | null | 关闭回调函数 |

# 按钮 Button

```tsx
import { Button } from '@casstime/bricks';

<Button>按钮</Button>
```

## 类型

按钮类型分为 primary(默认)、secondary(次按钮)、outline(边线按钮)、dashed(虚线按钮)、link(链接)。

:::demo 按钮类型示例

```tsx
class Example extends React.Component {
  render() {
    return (
      <div className="button-demo">
        <Button>默认按钮</Button>
        <Button secondary>次级按钮</Button>
        <Button outline>边线按钮</Button>
        <Button dashed>虚线按钮</Button>
        <Button link>链接按钮</Button>
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

## 形状
- 按钮的默认形状带2px圆角，如果添加 noRadius 则圆角变成0，即矩形
- 如果添加 round 显示半圆按钮


<br>
:::demo 按钮形状示例
```tsx
class Example extends React.Component {
  render() {
    return (
      <div className="button-demo">
        <Button>常规按钮</Button>
        <Button noRadius>没有圆角</Button>
        <Button round>半圆按钮</Button>
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

## 大小

按钮分为特大(xLarge)、大(large)、中（默认）、小(small)及特小(xSmall)几种类型。

:::demo 按钮大小示例
```tsx
class Example extends React.Component {
  render() {
    return (
      <div className="button-demo">
        <Button xLarge>特大按钮</Button>
        <Button large>大按钮</Button>
        <Button>默认按钮</Button>
        <Button small>小按钮</Button>
        <Button xSmall>特小按钮</Button>
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::


## 胖按钮
一般按钮的高度和字体跟按钮的大小相关，但是如果设置了 fat,则按钮高度会膨胀至100%，可以用于某些需要和父组件保持高度一致的场景；
<br>
:::demo 按钮大小示例

```tsx
class Example extends React.Component {
  render() {
    return (
      <div className="button-demo" style={{ height: '50px'}}>
        <Button xLarge fat>特大的胖按钮</Button>
        <Button xSmall fat>特小的胖按钮</Button>
        <Button success fat>成功的胖按钮</Button>
        <Button outline fat>外框胖按钮</Button>
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

## 颜色

按钮有不同的色彩方案，代表不同的情感偏好

- 成功 (success)
- 警告 (warn)

<br>
:::demo 按钮颜色示例

```tsx
class Example extends React.Component {
  render() {
    return (
      <div className="button-demo">
        <Button>默认按钮</Button>
        <Button secondary>次级按钮</Button>
        <Button success>成功按钮</Button>
        <Button secondary success>次级成功按钮</Button>
        <Button warn>警告按钮</Button>
        <Button secondary warn>次级警告按钮</Button>
      </div>
    );
  }
}
ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::


## 宽度

按钮类型分为 长按钮、短按钮。

- 长按钮 (long), 给按钮增加 long 后，按钮的宽度会设置成 100%，这样可以通过父元素控制按钮的宽度
- 短按钮 (short) 短按钮没有设置最小宽度，一般比普通按钮要短

<br>
:::demo 按钮类型示例

```tsx
class Example extends React.Component {
  render() {
    return (
      <div className="button-demo">
        <Button long>长按钮</Button>
        <br/>
        <Button>默认按钮</Button>
        <br/>
        <Button short>短按钮</Button>
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

## 状态

按钮不可用时，需要添加 disabled
<br>
:::demo 按钮类型示例
```tsx
class Example extends React.Component {
  render() {
    return (
      <div className="button-demo">
        <Button disabled>禁用</Button>
        <Button disabled>禁用按钮</Button>
        <Button success disabled>success禁用</Button>
        <Button outline disabled>outline禁用</Button>
        <Button dashed disabled>dashed禁用</Button>
        <Button link disabled>text禁用</Button>
      </div>
    );
  }
}
ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

## href

当Button指定 href 属性后会渲染成`<a>`标签

:::demo 按钮类型示例
```tsx
class Example extends React.Component {
  render() {
    return (
      <div className="button-demo">
        <Button href="https://www.cassmall.com">开思商城</Button>
        <Button success href="https://www.cassmall.com">开思商城</Button>
        <Button link href="https://www.cassmall.com">开思商城</Button>
      </div>
    );
  }
}
ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::


## 拓展Button的DOM事件
:::demo 按钮类型示例
```tsx
class Example extends React.Component {
  onMouseOver(e) {
    console.log('onMouseOver', e)
  }

  onMouseOut(e) {
    console.log('onMouseOut', e)
  }

  onMouseDown(e) {
    console.log('onMouseDown', e)
  }

  render() {
    return (
      <div className="button-demo">
        <Button  
          onMouseOver={this.onMouseOver.bind(this)}
          onMouseOut={this.onMouseOut.bind(this)}
          onMouseDown={this.onMouseDown.bind(this)}
        >
          默认按钮
        </Button>
        <Button  
          href="https://www.cassmall.com"
          link
          onMouseOver={this.onMouseOver.bind(this)}
          onMouseOut={this.onMouseOut.bind(this)}
        >
          开思商城
        </Button>
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

## 所有属性
| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
|  onClick | React.MouseEventHandler | undefined | 点击事件 |
|  primary | boolean | true | 主按钮，默认 |
|  secondary | boolean | undefined | 次按钮  |
|  outline | boolean | undefined | 外框按钮 |
|  dashed | boolean | undefined | 虚线按钮 |
|  link | boolean | undefined | 链接按钮 |
|  round | boolean | undefined | 胶囊按钮 |
|  noRadius | boolean | undefined | 无圆角按钮 |
|  fat | boolean | undefined | 胖按钮，100%高 |
|  xLarge | boolean | undefined | 特大号按钮 |
|  large | boolean | undefined | 大号按钮 |
|  small | boolean | undefined | 小号按钮 |
|  xSmall | boolean | undefined | 特小号按钮 |
|  long | boolean | undefined | 长按钮 |
|  short | boolean | undefined | 短按钮 |
|  success | boolean | undefined | 成功按钮 |
|  warn | boolean | undefined | 警告按钮 |
|  disabled | boolean | undefined | 禁用按钮 |
|  href | string | undefined | 链接，指定后会渲染成`<a>`标签 |
|  className | string | undefined | 自定义className |
|  style | React.CSSProperties | undefined | 自定义style |

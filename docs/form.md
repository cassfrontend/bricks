# form 表单

```jsx
import { FormGroup, FormLabel, FormControl, FormHelp } from '@casstime/bricks';

<FormGroup>
  <FormLabel>姓名：</FormLabel>
  <FormControl><Input /></FormControl>
  <FormHelp icon="check-circle">输入正确</FormHelp>
</FormGroup>
```

## FormGroup

FormGroup指单条表单，可以在FromGroup中为表单设置大小(设置后只影响FormLabel，FormHelp,后续将会影响Input,Select等)，也可以设置校验状态

FormGroup 继承了Row，所以可以在FormGroup中直接使用Col

| 属性 | 类型 | 描述 |
|-----|------|-----|
|  error | boolean | 校验失败 |
|  warn | boolean | 警告 |
|  success | boolean | 校验通过 |
|  xSmall | boolean | 特小表单 |
|  small | boolean | 小表单 |
|  large | boolean | 大表单 |
|  xLarge | boolean | 特大表单 |

:::demo
```jsx
class Demo extends React.Component {
  render() {
    return (
      <form>
        <FormGroup error>
          <FormLabel>姓名：</FormLabel>
          <FormControl><Input /></FormControl>
          <FormHelp icon="warn-circle">输入错误</FormHelp>
        </FormGroup>

        <FormGroup success>
          <Col sm={2}>
            <FormLabel align="right">姓名：</FormLabel>
          </Col>
          <Col sm={5}>
            <FormControl><Input /></FormControl>
          </Col>
          <Col sm={5}>
            <FormHelp icon="check-circle">输入错误</FormHelp>
          </Col>
        </FormGroup>
      </form>
    )
  }
}

```
:::

## FormLabel

| 属性 | 类型 | 描述 |
|-----|------|-----|
|  align | 'left' \| 'center' \| 'right' | 标签中文本对齐方式 |

## FormControl 

里面可以包裹表单控件，如Input,Select等

## FormHelp

表单提示信息

| 属性 | 类型 | 描述 |
|-----|------|-----|
|  icon | string | 提示图标类型 |


## 表单校验

我们可能会在不同的时机校验表单，比如点击提交按钮，输入完成等...也可能每次触发校验时，只校验某几条数据，

所以设计表单校验功能的时候，尽可能提供灵活的使用方式。

```jsx
import { withValidator } from '@casstime/bricks';
```

:::demo 示例

```jsx

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      name: '',
      email: '',
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.props.setValues(() => ({
      name: this.state.name,
      email: this.state.email,
    }));
  }

  handleNameChange(event) {
    this.setState({ name: event.target.value });
  }
  
  handleEmailChange(event) {
    this.setState({ email: event.target.value });
  }

  handleBlur(event) {
    this.props.validate();
  }

  handleFocus(event) {
    this.props.clearError();
  }

  render() {
    const { getError, getSuccess, validations } = this.props;

    console.log(validations);
    return (
      <form>
        <FormGroup>
          <Col sm={2} className="align-right">
            <FormLabel align="right">姓名：</FormLabel>
          </Col>
          <Col sm={5}>
            <FormControl>
              <Input 
                value={this.state.name}
                error={getError('name')}
                onChange={this.handleNameChange}
                onBlur={this.handleBlur}
                onFocus={this.handleFocus}
              />
            </FormControl>
          </Col>
          <Col sm={5}>
            {
              getError('name') && <FormHelp error icon="warn-circle">内容必须包含张三</FormHelp>
            }
            {
              getSuccess('name') && <FormHelp success icon="check-circle">恭喜，输入了张三</FormHelp>
            }
          </Col>
        </FormGroup>

        <FormGroup>
          <Col sm={2} className="align-right">
            <FormLabel align="right">邮箱：</FormLabel>
          </Col>
          <Col sm={5}>
            <FormControl>
              <Input 
                value={this.state.email}
                error={getError('email')}
                onChange={this.handleEmailChange}
                onBlur={this.handleBlur}
                onFocus={this.handleFocus}
              />
            </FormControl>
          </Col>
          <Col sm={5}>
            {
              getError('email') && <FormHelp error icon="warn-circle">{`邮箱必须为${this.state.name}@casstime.com`}</FormHelp>
            }
            {
              getSuccess('email') && <FormHelp success icon="check-circle">{`恭喜，输入了${this.state.name}@casstime.com`}</FormHelp>
            }
          </Col>
        </FormGroup>

      </form>
    )
  }
}

const Demo = withValidator({
  name: /张三/,
  email: (value, values) => {
    console.log('校验字段及值', values);
    if (value === values.name + '@casstime.com') {
      return {
        error: false,
        success: true
      };
    }
    return {
      error: '邮箱校验失败',
      success: false
    };
  }
})(Form);

```
:::

### Validation 校验结果

| 属性 | 类型 | 描述 |
|-----|------|-----|
|  error | boolean\|string | 校验失败 |
|  warn | boolean\|string | 警告 |
|  success | boolean\|string | 校验通过 |

### Rule 校验规则

校验规则可以是正则表达式，也可以是返回校验结果(Validation)的函数(拥有俩个参数，第一个参数为当前校验字段的值，第二个参数为全部校验字段以及值组成的对象)

```js
const rules = {
  name: /^张三$/,
  phone: /\d{11}/,
  email: (value, values) => {
    if (value === 'email@casstime.com') {
      return {
        error: false,
        success: true
      };
    }
    return {
      error: '邮箱校验失败',
      success: false
    };
  }
};
```

### withValidator(rules)(Component)


```jsx
const rules = {
  name: /^张三$/,
  phone: /\d{11}/
};
const ValidatorForm = withValidator(rules)(Form);
```

### ValidatorProps

使用withValidator后，将会往Component组件中注入props

| 属性 | 类型 | 描述 |
|-----|------|-----|
|  setValues | Function(valueGetter) | 设置校验的取值方式，需要在调用validate前设置，如 setValues(() => ({ name: this.state.name })) |
|  validate | Function(args) | 触发校验，参数为空时，校验所有数据，否则校验部分数据， validate('name', 'email') |
|  clearError | Function(args) | 清空制定的校验结果，如果不传，清空所有校验结果 |
|  getError | Function(key) | 返回validation中的error值 |
|  getSuccess | Function(key) | 返回validation中的success值 |
|  getWarn | Function(key) | 返回validation中的warn值 |
|  validations | { key: Validation } | 所有的校验结果  |





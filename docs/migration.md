# 变更说明（V0.20.0 --> V0.21.0）

#### ⚡️为了使Tabs组件更方便用户使用、封装、拓展、并且适用于更多的业务场景，​我们在v0.21.0版本中对Tabs组件进行了重构。

#### ⚡️升级到v0.21.0版本之后，Tabs组件与之前版本的Tabs版本代码实现方式不一致，从V0.21.0版本开始，将不兼容之前版本的Tabs组件。

#### ⚡为了避免升级bricks版本之后对依赖项目因不兼容而造成的影响，本文档将引导使用者在升级版本之前，如何更改代码，避免不兼容的情况发生。


## 影响范围

#### ⚡所有使用到bricks组件库中Tabs组件的项目

## 对比说明

#### 💄V0.20.0 版本使用示例： https://cassfrontend.gitee.io/bricks-docs/v0.20.0/#/tabs
```tsx

import { Tabs } from '@casstime/bricks';

 <Tabs
    defaultActiveKey={this.state.tabs.defaultActiveKey}
    activeKey={this.state.tabs.activeKey}
    onChange={this.onChange}
  >
    <Tabs.Pane tab="tab 1" key="1">first</Tabs.Pane>
    <Tabs.Pane tab="tab 2" key="2">second</Tabs.Pane>
    <Tabs.Pane tab="tab 3" key="3">third</Tabs.Pane>
  </Tabs>

```

#### 💄V0.21.0 版本使用示例： https://cassfrontend.gitee.io/bricks-docs/v0.21.0/#/tabs

```tsx

import { Tabs , Tab, TabContainer} from '@casstime/bricks';

<Tabs
  activeKey={this.state.activeKey}
  onChange={(value) => this.onChange(value)}
>
  <Tab value="1" >tab 1</Tab>
  <Tab value="2" >tab 2</Tab>
  <Tab value="3" >tab 3</Tab>
</Tabs>
<TabContainer>
  {this.state.activeKey === '1' && <div> first</div>}
  {this.state.activeKey === '2' && <div> second</div>}
  {this.state.activeKey === '3' && <div> third</div>}
</TabContainer>

```

## 示例（以V0.20.0-->V0.21.0版本为例）

#### 一般使用
:::demo

```tsx

interface ITabsProps {
  activeKey?: string;
  onChange: (activeKey: string) => void;
  className?: string;
  style?: React.CSSProperties;
}

interface ITabsState {
  activeKey?: string;
}

class Demo extends React.Component<ITabsProps, ITabsState> {
  constructor(props: any) {
    super(props);
    this.state = {
      activeKey: '1',
    };

    this.onChange = this.onChange.bind(this);
  }
  public onChange(activeKeyIndex: string) {
    this.setState(() => {
      return {
        activeKey: activeKeyIndex,
      };
    });
  }

  public render() {
    return (
      <div>
        <Tabs
          activeKey={this.state.activeKey}
          onChange={(value: string) => this.onChange(value)}
        >
          <Tab value="1" >tab 1</Tab>
          <Tab value="2" >tab 2</Tab>
          <Tab value="3" >tab 3</Tab>
        </Tabs>
        <TabContainer>
          {this.state.activeKey === '1' && <div> first</div>}
          {this.state.activeKey === '2' && <div> second</div>}
          {this.state.activeKey === '3' && <div> third</div>}
        </TabContainer>
      </div >
    );
  }
}
```
:::

#### 💄V0.20.0代码:
```tsx

import { Tabs } from '@casstime/bricks';

interface IDemoState {
  defaultActiveKey?: string;
  activeKey?: string;
}

class Demo extends React.Component<{}, IDemoState> {

  constructor(props: {}) {
    super(props);
    this.state = {
      defaultActiveKey: '2',
      activeKey: '1' ,
    };
    this.onChange = this.onChange.bind(this);
  }
  public onChange(activeKeyIndex: string) {
    this.setState({
      activeKey: activeKeyIndex,
    });
  }

  public render() {
    return (
    <Tabs
      defaultActiveKey={this.state.defaultActiveKey}
      activeKey={this.state.activeKey}
      onChange={this.onChange}
    >
      <Tabs.Pane tab="tab 1" key="1">first</Tabs.Pane>
      <Tabs.Pane tab="tab 2" key="2">second</Tabs.Pane>
      <Tabs.Pane tab="tab 3" key="3">third</Tabs.Pane>
    </Tabs>
    );
  }
}

```

#### 💄V0.21.0代码:
```tsx

import { Tabs, Tab, TabContainer } from '@casstime/bricks';

interface ITabsProps {
  activeKey?: string;
  onChange: (activeKey: string) => void;
  className?: string;
  style?: React.CSSProperties;
}

interface ITabsState {
  activeKey?: string;
}

class Demo extends React.Component<ITabsProps, ITabsState> {
  constructor(props: any) {
    super(props);
    this.state = {
      activeKey: '1',
    };

    this.onChange = this.onChange.bind(this);
  }
  public onChange(activeKeyIndex: string) {
    this.setState(() => {
      return {
        activeKey: activeKeyIndex,
      };
    });
  }

  public render() {
    return (
      <div>
        <Tabs
          activeKey={this.state.activeKey}
          onChange={(value: string) => this.onChange(value)}
        >
          <Tab value="1" >tab 1</Tab>
          <Tab value="2" >tab 2</Tab>
          <Tab value="3" >tab 3</Tab>
        </Tabs>
        <TabContainer>
          {this.state.activeKey === '1' && <div> first</div>}
          {this.state.activeKey === '2' && <div> second</div>}
          {this.state.activeKey === '3' && <div> third</div>}
        </TabContainer>
      </div >
    );
  }
}

```




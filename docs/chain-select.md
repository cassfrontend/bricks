# 级联组件 ChainSelect

```tsx
import { ChainSelect, IChainSelectOption } from '@casstime/bricks';

<ChainSelect options={[{label: '广东', value: 'guangdong'}, {label: '北京', value: 'beijing'}]} />
```

### 一般使用
:::demo
```tsx
class Demo extends React.Component {
  
  onChange(value) {
    console.log('value', value);
  }

  render() {
    return (
      <div>
        <ChainSelect 
          options={options} 
          onChange={(value) => this.onChange(value)}
        />
        <ChainSelect 
          options={options} 
          titles={[
            { value: '', label: '省' },
            { value: '', label: '市' },
            { value: '', label: '区' },
          ]}
          onChange={(value) => this.onChange(value)}
        />
        <ChainSelect 
          options={options}
          titles={[
            { value: '', label: '省' },
            { value: '', label: '市' },
            { value: '', label: '区' },
            { value: '', label: '街道' },
          ]}
          onChange={(value) => this.onChange(value)}
        />
      </div>
    );
  }
}

const options = [
  {
    label: '广东',
    value: 'guangdong',
    children: [
      {
        label: '深圳',
        value: 'shenzhen',
        children: [
          {
            label: '龙岗',
            value: 'longgangqu',
            children: [
              {
                label: '坂田',
                value: 'bantian',
              }, 
              {
                label: '五和',
                value: 'wuhe',
              },
            ],
          }, 
          {
            label: '南山',
            value: 'nanshanqu',
            children: [
              {
                label: '下沙',
                value: 'xiasha',
              }, 
              {
                label: '深南',
                value: 'shennan',
              },
            ],
          },
        ],
      }, 
      {
        label: '广州',
        value: 'guangzhou',
        children: [
          {
            label: '白云',
            value: 'baiyunqu',
          },
        ],
      },
    ],
  }, 
  {
    label: '浙江',
    value: 'Zhejiang',
    children: [
      {
        label: '杭州',
        value: 'Hangzhou',
        children: [
          {
            label: '西湖',
            value: 'West Lake',
          },
        ],
      },
    ],
  }, 
  {
    label: '江苏',
    value: 'Jiangsu',
    children: [
      {
        label: '南京',
        value: 'Nanjing',
        children: [
          {
            label: '中华门',
            value: 'Zhong Hua Men',
          },
        ],
      },
    ],
  }, 
  {
    label: '北京',
    value: 'beijin',
  },
];
```
:::

### 受控使用的情况
:::demo
```tsx
class Demo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: []
    }
  }

  onChange(value) {
    console.log('value',value)
    this.setState({
      value: value,
    })
  }

  render() {
    const {value} = this.state;
    return (
      <ChainSelect 
        options={options} 
        titles={titles}
        onChange={(value) => this.onChange(value)}
        value={value}
      />
    );
  }
}

const titles = [
  { value: '', label: '省' },
  { value: '', label: '市' },
  { value: '', label: '区' },
  { value: '', label: '街道' },
];

const options = [
  {
    label: '广东',
    value: 'guangdong',
    children: [
      {
        label: '深圳',
        value: 'shenzhen',
        children: [
          {
            label: '龙岗',
            value: 'longgangqu',
            children: [
              {
                label: '坂田',
                value: 'bantian',
              }, 
              {
                label: '五和',
                value: 'wuhe',
              },
            ],
          }, 
          {
            label: '南山',
            value: 'nanshanqu',
            children: [
              {
                label: '下沙',
                value: 'xiasha',
              }, 
              {
                label: '深南',
                value: 'shennan',
              },
            ],
          },
        ],
      }, 
      {
        label: '广州',
        value: 'guangzhou',
        children: [
          {
            label: '白云',
            value: 'baiyunqu',
          },
        ],
      },
    ],
  }, 
  {
    label: '浙江',
    value: 'Zhejiang',
    children: [
      {
        label: '杭州',
        value: 'Hangzhou',
        children: [
          {
            label: '西湖',
            value: 'West Lake',
          },
        ],
      },
    ],
  }, 
  {
    label: '江苏',
    value: 'Jiangsu',
    children: [
      {
        label: '南京',
        value: 'Nanjing',
        children: [
          {
            label: '中华门',
            value: 'Zhong Hua Men',
          },
        ],
      },
    ],
  }, 
  {
    label: '北京',
    value: 'beijin',
  },
];
```
:::

### 有默认值的情况
:::demo
```tsx
class Demo extends React.Component {

  onChange(value) {
    console.log('选择的区域为', value)
  }

  render() {
    return (
      <ChainSelect 
        options={options} 
        titles={titles}
        onChange={(value) => this.onChange(value)}
        defaultValue={defaultValue}
      />
    );
  }
}

const titles = [
  { value: '', label: '省' },
  { value: '', label: '市' },
  { value: '', label: '区' },
  { value: '', label: '街道' },
];

const defaultValue = [
  { value: 'guangdong', label: '广东' },
  { value: 'shenzhen', label: '深圳' },
  { value: 'longgangqu', label: '龙岗' },
  { value: 'bantian', label: '坂田' },
];

const options = [
  {
    label: '广东',
    value: 'guangdong',
    children: [
      {
        label: '深圳',
        value: 'shenzhen',
        children: [
          {
            label: '龙岗',
            value: 'longgangqu',
            children: [
              {
                label: '坂田',
                value: 'bantian',
              }, 
              {
                label: '五和',
                value: 'wuhe',
              },
            ],
          }, 
          {
            label: '南山',
            value: 'nanshanqu',
            children: [
              {
                label: '下沙',
                value: 'xiasha',
              }, 
              {
                label: '深南',
                value: 'shennan',
              },
            ],
          },
        ],
      }, 
      {
        label: '广州',
        value: 'guangzhou',
        children: [
          {
            label: '白云',
            value: 'baiyunqu',
          },
        ],
      },
    ],
  }, 
  {
    label: '浙江',
    value: 'Zhejiang',
    children: [
      {
        label: '杭州',
        value: 'Hangzhou',
        children: [
          {
            label: '西湖',
            value: 'West Lake',
          },
        ],
      },
    ],
  }, 
  {
    label: '江苏',
    value: 'Jiangsu',
    children: [
      {
        label: '南京',
        value: 'Nanjing',
        children: [
          {
            label: '中华门',
            value: 'Zhong Hua Men',
          },
        ],
      },
    ],
  }, 
  {
    label: '北京',
    value: 'beijin',
  },
];
```
:::

### 可筛选的情况
:::demo
```tsx
class Demo extends React.Component {
  onChange(value) {
    console.log('选择的区域为', value)
  }

  render() {
    return (
      <ChainSelect 
        options={options} 
        titles={titles}
        onChange={(value) => this.onChange(value)}
        searchable
      />
    );
  }
}

const titles = [
  { value: '', label: '省' },
  { value: '', label: '市' },
  { value: '', label: '区' },
  { value: '', label: '街道' },
];

const options = [
  {
    label: '广东',
    value: 'guangdong',
    children: [
      {
        label: '深圳',
        value: 'shenzhen',
        children: [
          {
            label: '龙岗',
            value: 'longgangqu',
            children: [],
          }, 
          {
            label: '南山',
            value: 'nanshanqu',
            children: [],
          },
        ],
      }, 
      {
        label: '广州',
        value: 'guangzhou',
        children: [
          {
            label: '白云',
            value: 'baiyunqu',
          },
        ],
      },
    ],
  }, 
  {
    label: '浙江',
    value: 'Zhejiang',
    children: [
      {
        label: '杭州',
        value: 'Hangzhou',
        children: [
          {
            label: '西湖',
            value: 'West Lake',
          },
        ],
      },
    ],
  }, 
  {
    label: '江苏',
    value: 'Jiangsu',
    children: [
      {
        label: '南京',
        value: 'Nanjing',
        children: [
          {
            label: '中华门',
            value: 'Zhong Hua Men',
          },
        ],
      },
    ],
  }, 
  {
    label: '北京',
    value: 'beijin',
  },
];
```
:::

### 选项异步加载
:::demo
```tsx
class Demo extends React.Component {
  constructor(props: {}) {
    super(props);
    this.state = {
      options: provinceOptions,
    }
    this.onChange = this.onChange.bind(this);
    this.loadData = this.loadData.bind(this);
  }

  onChange(value) {
    console.log('选择的区域为', value)
  }

  loadData(targetOption) {
    const {options} = this.state;
    const that = this;
    let promise = new Promise(function(resolve, reject) {
      setTimeout(() => {
          targetOption.children = [
            {
              label: `${targetOption.label} 1`,
              value: `${targetOption.value}1`,
            }, 
            {
              label: `${targetOption.label} 2`,
              value: `${targetOption.value}2`,
            }
          ];
          that.setState({
            options: [...that.state.options],
          })
          resolve(targetOption);
      }, 200)
    });
    return promise;
  }

  render() {
    const {options, loaded} = this.state;
    return (
      <ChainSelect 
        options={options} 
        titles={titles}
        onChange={(value) => this.onChange(value)}
        loadData={this.loadData}
      />
    );
  }
}

const titles = [
  { value: '', label: '省' },
  { value: '', label: '市' },
  { value: '', label: '区' },
  { value: '', label: '街道' },
];

const provinceOptions = [
  {
    label: '广东',
    value: 'guangdong',
  }, 
  {
    label: '浙江',
    value: 'Zhejiang',
    children: [
      {
        label: '杭州',
        value: 'Hangzhou',
        children: [
          {
            label: '西湖',
            value: 'West Lake',
          },
        ],
      },
    ],
  }, 
  {
    label: '江苏',
    value: 'Jiangsu',
    children: [
      {
        label: '南京',
        value: 'Nanjing',
        children: [
          {
            label: '中华门',
            value: 'Zhong Hua Men',
          },
        ],
      },
    ],
  },
];
```
::: 

## ChainSelect 组件 props 描述
| 属性 | 类型 | 默认值| 描述 |
| ---- | ----| ---- | ---- |
| className | string | '' | 自定义className |
| style | React.CSSProperties | {} | 自定义style |
| titles | Option[] | [{ value: '', label: '省' }, { value: '', label: '市' }] | 确定级联数和标题未选时显示 |
| options | ISelectOption[] | [] | 选项内容 | 选项内容 |
| value | IChainSelectOption[] | undefined | 传入则组件受控，显示传入的内容 |
| defaultValue | IChainSelectOption[] | [] | 级联选择默认值 |
| searchable | boolean | false | 下拉框是否可筛选 |
| onChange | function: (value: IChainSelectOption[]) => void | (value) => void | 选择完成后的回调 |
| loadData | function: (value: IChainSelectOption) => Promise<IChainSelectOption> | undefined | 动态加载选项 |
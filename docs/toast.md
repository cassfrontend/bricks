
# 弹出窗 Toast

```tsx
import { Toast } from '@casstime/bricks';
```

### 一般使用

:::demo typescript示例
```tsx
interface IDemoState {
  icon?: string;
  description?: string;
  color?: string;
  visible?: boolean;
}

class Demo extends React.Component<{}, IDemoState> {

  constructor(props:{}) {
    super(props);
    this.state = {
      icon: "check-circle",
      description: "保存成功~~",
      color: "#43b52c",
    }
    this.showModal = this.showModal.bind(this);
  }

  showModal () {
    this.setState({
      visible: true,
    });
    const timer = setInterval(() => {
      this.setState({
      visible: false,
    });
        clearInterval(timer);
      }, 2000);
  }

  render() {
    return (
      <div>
        <Button onClick={this.showModal}>Open</Button>
        {this.state.visible ? <Toast
          icon = {this.state.icon}
          color = {this.state.color}
        >
        {this.state.description}
        </Toast> : null}
      </div>
    )
  }
}

```
:::

### 函数调用：

```tsx
  // 方法一
  Toast.warn(text: string, duration?: number);
  Toast.success(text: string, duration?: number);
  Toast.error(text: string, duration?: number);
  Toast.loading(text: string, duration?: number);
  Toast.hide();

  // 方法二
  const toast = Toast.loading();
  toast.hide();

```

:::demo typescript示例

```tsx
class Demo extends React.Component<{} , {}> {

  // 成功（方法一）
  success () {
    Toast.success('保存成功！', 2000);
  }

  // 错误（方法一）
  error () {
    Toast.error('信息有误', 2000);
  }

  // 警告（方法二）
  warn () {
    const toast = Toast.warn('信息有误');
    setTimeout(() => {
     toast.hide();
    }, 2000);
  }

  // 加载中（方法二）
  loading () {
    const toast = Toast.loading('内容正在加载中');
    setTimeout(() => {
     toast.hide();
    }, 2000);
  }

  render() {
    return (
      <Row>
        <Col xs={6} md={4} lg={3}><Button onClick={this.success.bind(this)} success>success</Button></Col>
        <Col xs={6} md={4} lg={3}><Button onClick={this.error.bind(this)}>error</Button></Col>
        <Col xs={6} md={4} lg={3}><Button onClick={this.warn.bind(this)} warn>warn</Button></Col>
        <Col xs={6} md={4} lg={3}><Button secondary onClick={this.loading.bind(this)}>loading</Button></Col>
      </Row>
    )
  }
}
```
:::


## 参数描述
| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
|  text | string | '' | 弹窗文字 |
| duration | number | '' | （可选）时间间隔 |
| className | string | '' | 自定义 className |
| style | React.CSSProperties | undefined | 自定义样式 |





## 图标 Icon

```tsx
import { Icon } from '@casstime/bricks';

<Icon type="search" />
```

### 图标命名规范
- 命名顺序：icon-[图标含义]-[形状?]-[描线?]-[方向?] （?为可选）
- 实心和描线图标保持同名，用 -o 来区分，比如 icon-question-circle（实心） 和 icon-question-circle-o（描线）
- 相同含义图标保持同名，用字母来区分，比如 icon-edit-a 和 icon-edit-b

### 一般使用
通过添加类名`icon` 和 `图标对应的名称` 来使用即可。
:::demo
```jsx
 class Demo extends React.Component{
  
  render() {
    return (
      <Icon
        type="search"
        size={12} 
        color="#888"
      />
    )
  }
}
```
:::

### 拓展Icon的DOM事件
:::demo
```jsx
 class Demo extends React.Component {
  
  onMouseOver(e) {
    console.log('onMouseOver', e)
  }

  onMouseOut(e) {
    console.log('onMouseOut', e)
  }

  onMouseDown(e) {
    console.log('onMouseDown', e)
  }

  render() {
    return (
      <Icon
        type="search"
        size={12} 
        color="#888"
        onMouseOver={this.onMouseOver.bind(this)}
        onMouseOut={this.onMouseOut.bind(this)}
        onMouseDown={this.onMouseDown.bind(this)}
      />
    )
  }
}
```
:::



## Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| type | string | null | 图标名称（必填） |
| size | number | null | 自定义图标大小 |
| color | string | null | 自定义图标颜色 |
| className | string | null | 自定义className |
| onClick | React.MouseEventHandler | undefined | 点击事件 |
| style | object | null | 自定义样式 |


### 图标列表

<div class="icon-list">
:::demo
```js-icon
{
  "方向类图标": [
    { "iconName": "icon-hand-left", "tag": "" },
    { "iconName": "icon-hand-right", "tag": "" },
    { "iconName": "icon-arrow-down", "tag": "" },
    { "iconName": "icon-arrow-up", "tag": "" },
    { "iconName": "icon-left", "tag": "" },
    { "iconName": "icon-right", "tag": "" },
    { "iconName": "icon-up", "tag": "" },
    { "iconName": "icon-down", "tag": "" },
  ],
  "提示类图标": [
    { "iconName": "icon-question-circle", "tag": "" },
    { "iconName": "icon-warn-triangle", "tag": "" },
    { "iconName": "icon-info-circle", "tag": "" },  
    { "iconName": "icon-check-circle", "tag": "" },
    { "iconName": "icon-check-circle-o", "tag": "" },  
    { "iconName": "icon-warn-circle", "tag": "" },  
    { "iconName": "icon-warn-circle-o", "tag": "" },  
    { "iconName": "icon-info-solid", "tag": "" },  
    { "iconName": "icon-info-solid-o", "tag": "" },  
  ],
  "操作类图标": [
    { "iconName": "icon-close", "tag": "" },
    { "iconName": "icon-close-thin", "tag": "" },
    { "iconName": "icon-edit-a", "tag": "" },
    { "iconName": "icon-edit-b", "tag": "" },
    { "iconName": "icon-trash", "tag": "" },
    { "iconName": "icon-download", "tag": "" },
    { "iconName": "icon-check", "tag": "" },
    { "iconName": "icon-round-check", "tag": "" },
    { "iconName": "icon-add", "tag": "" },
    { "iconName": "icon-subtract", "tag": "" },
    { "iconName": "icon-close-solid", "tag": "" },
    { "iconName": "icon-close-solid-o", "tag": "" },  
    { "iconName": "icon-transfer", "tag": "" },
    { "iconName": "icon-add-case", "tag": "" },
    { "iconName": "icon-subtract-case", "tag": "" },
    { "iconName": "icon-unselected", "tag": "" },
    { "iconName": "icon-selected", "tag": "" },
    { "iconName": "icon-back", "tag": "" },
    { "iconName": "icon-search", "tag": "" },
    { "iconName": "icon-right-thin", "tag": "" },
    { "iconName": "icon-refresh", "tag": "" }
  ],
  "标识类图标": [
    { "iconName": "icon-board-circle", "tag": "" },
    { "iconName": "icon-winter", "tag": "" },
    { "iconName": "icon-date", "tag": "" },
    { "iconName": "icon-car", "tag": "" },
    { "iconName": "icon-eye", "tag": "" },
    { "iconName": "icon-star", "tag": "" },
    { "iconName": "icon-star-o", "tag": "" }
  ]  
}
```
:::
</div>
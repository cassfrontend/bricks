# Tooltip

```tsx
import { Tooltip } from '@casstime/bricks';
```

### 一般使用
:::demo Tooltip组件示例

```jsx
class Demo extends React.Component {
  render() {
    const styles = {
      display: 'table-cell',
      height: '60px',
      width: '80px',
      textAlign: 'center',
      background: '#f6f6f6',
      verticalAlign: 'middle',
      border: '5px solid white',
      textDecoration: 'none',
    };
    return (
      <div>
        <Tooltip 
          placement="left" 
          trigger={['hover']} 
          overlay={<span>left</span>}
          arrowContent={<div className="rc-tooltip-arrow-inner"></div>}
        >
          <div style={styles}>div</div>
        </Tooltip>
        <Tooltip 
          placement="top" 
          trigger={['hover']} 
          overlay={<span>top</span>}
          arrowContent={<div className="rc-tooltip-arrow-inner"></div>}
        >
          <span href="#" style={styles}>span</span>
        </Tooltip>
        <Tooltip 
          placement="bottom" 
          trigger={['hover']} 
          overlay={<span>bottom</span>}
          arrowContent={<div className="rc-tooltip-arrow-inner"></div>}
        >
          <div style={styles}><Text type="smallTitle" color="success" bold>Text组件</Text></div>
        </Tooltip>
        <Tooltip 
          placement="right" 
          trigger={['hover']} 
          overlay={<span>right</span>}
          arrowContent={<div className="rc-tooltip-arrow-inner"></div>}
        >
          <div style={{display: 'inline-block'}}><Button outline>边线按钮</Button></div>
        </Tooltip>
      </div>
    );
  }
}

```
:::

## Tooltip Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| placement | String | &nbsp; | ['left','right','top','bottom', 'topLeft', 'topRight', 'bottomLeft', 'bottomRight']其中之一 |
| trigger | string[] | ['hover'] | ['hover','click','focus']其中之一 |
| overlay | React.Element | () => React.Element | &nbsp; | tip提示文本 |
| visible | boolean | &nbsp; | 是否可见 |
| defaultVisible | boolean | &nbsp; | Tooltip初始化时是否可见 |
| onVisibleChange | Function | &nbsp; | 可见状态改变时的回调 |
| overlayStyle | Object | &nbsp; | 自定义Tooltip的样式 |
| overlayClassName | string | &nbsp; | 自定义Tooltip的类名 |
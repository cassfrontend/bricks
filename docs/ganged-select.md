# 级联选择框

:::demo 示例

```tsx
interface IExampleObject {
  value: string;
  label: string;
}
interface IExampleState {
  defaultValues: IExampleObject[];
  titles: IExampleObject[];
}
class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
        defaultValues: [{ value: '2', label: '湖南' }, { value: '2-1', label: '长沙' }],
        titles: [{ value: '0', label: '省份' }, { value: '0-0', label: '城市' }],
    }
    this.loadData = this.loadData.bind(this);
    this.onSelect = this.onSelect.bind(this);
  }
  // 获取数据源
  loadData(index: number, id: string) {
    return new Promise(function(resolve,reject) {
      let optinos = [];
        switch (id) {
          case '1' :
              optinos =  [{ value: '1-1', label: '广州'},
              { value: '1-2', label: '深圳'},
              { value: '1-3', label: '东莞' }]
              break;
          case '2' :
              optinos =  [{ value: '2-1', label: '长沙'},
              { value: '2-2', label: '益阳'},
              { value: '2-3', label: '湘潭' }]
              break;
          case '3' :
              optinos =  [{ value: '3-1', label: '武汉'},
              { value: '3-2', label: '宜昌'},
              { value: '3-3', label: '襄阳' }]
              break;
          default: 
              optinos =  [{ value: '1', label: '广东'},
              { value: '2', label: '湖南'},
              { value: '3', label: '湖北' }]
          }
          resolve(optinos);
      })
    }
    
    onSelect(values: IExampleObject) {
      console.log(values);
    }
  render() {
    return (
      <GangedSelect
        defaultValues={this.state.defaultValues} // 默认值
        titles={this.state.titles} // 配置几级  每一级的标题
        loadData={this.loadData}
        onSelect={this.onSelect}
      />
    );
  }
}
ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))

```
:::

## 可筛选级联选择框

:::demo 示例

```tsx
interface IExampleObject {
  value: string;
  label: string;
}
interface IExampleState {
  defaultValues: IExampleObject[];
  titles: IExampleObject[];
}
class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
        defaultValues: [{ value: '2', label: '湖南' }, { value: '2-1', label: '长沙' }],
        titles: [{ value: '0', label: '省份' }, { value: '0-0', label: '城市' }],
    }
    this.loadData = this.loadData.bind(this);
    this.onSelect = this.onSelect.bind(this);
  }
  // 获取数据源
  loadData(index: number, id: string) {
    return new Promise(function(resolve,reject) {
      let optinos = [];
        switch (id) {
          case '1' :
              optinos =  [{ value: '1-1', label: '广州'},
              { value: '1-2', label: '深圳'},
              { value: '1-3', label: '东莞' }]
              break;
          case '2' :
              optinos =  [{ value: '2-1', label: '长沙'},
              { value: '2-2', label: '益阳'},
              { value: '2-3', label: '湘潭' }]
              break;
          case '3' :
              optinos =  [{ value: '3-1', label: '武汉'},
              { value: '3-2', label: '宜昌'},
              { value: '3-3', label: '襄阳' }]
              break;
          default: 
              optinos =  [{ value: '1', label: '广东'},
              { value: '2', label: '湖南'},
              { value: '3', label: '湖北' }]
          }
          resolve(optinos);
      })
    }
    
    onSelect(values: IExampleObject) {
      console.log(values);
    }
  render() {
    return (
      <GangedSelect searchable
        defaultValues={this.state.defaultValues} // 默认值
        titles={this.state.titles} // 配置几级  每一级的标题
        loadData={this.loadData}
        onSelect={this.onSelect}
      />
    );
  }
}
ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))

```
:::


## Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| titles | Option[] | [{ value: '0', label: '省份' }, { value: '0-0', label: '城市' }] | 确定联数和标题 |
| defaultValues | Option[] | null | 默认选中的参数 |
| values | Option[][] | null | 所有列的数据集合 |
| searchable | boolean | false | 下拉框是否可筛选 |
| loadData | (depth: number, parentValue?: string) => Promise<Option[]>; | - | 获取当前联的数据 |
| onSelect | (Option[]) => void	 | - | 选择完成后的回调 |

### Option 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| label | string | '' | 选项显示文本 |
| value | string | '' | 选项的值 |

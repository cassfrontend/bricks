# 进度条 progress
```tsx
import { Progress } from '@casstime/bricks';

<Progress />
<Progress percent={30} />
```
### 一般使用 
:::demo
```tsx
class Demo extends React.Component {
  
  render() {
    return (
      <div>
        <Progress percent={30} />
        <Progress type='circle' percent={30} />
      </div>
    );
  }
}
```
:::

### 不同颜色进度条
:::demo
```tsx
class Demo extends React.Component{
  render() {
    return (
      <div>
        <Progress percent={60} strokeColor='#ffa641' />
        <Progress percent={60} strokeColor='#ffa641' trailColor='rgb(255, 166, 65, .2)' />
        <Progress percent={60} trailColor='#fff' />
        <Progress percent={60} trailColor='rgb(52, 195, 255, .2)' />
        <Progress type='circle' percent={60} strokeColor='#ffa641' />
        <Progress type='circle' percent={60} strokeColor='#ffa641' trailColor='rgb(255, 166, 65, .2)'/>
        <Progress type='circle' percent={60} trailColor='#fff' />
        <Progress type='circle' percent={60} trailColor='rgb(52, 195, 255, .2)' />
      </div>
    );
  }
}
```
:::

### 不同尺寸进度条
:::demo
```tsx
class Demo extends React.Component {
  render() {
    return (
      <div>
        <Progress percent={30} strokeWidth='4' />
        <Progress percent={30} strokeWidth='12' />
        <Progress type='circle' percent={30} strokeWidth='4' />
        <Progress type='circle' percent={30} strokeWidth='12' />
        <Progress type='circle' percent={30} width={80} height={80}/>
        <Progress type='circle' percent={30} width={120} height={120}/>
      </div>
    );
  }
}
```
:::

### 动态显示进度条
:::demo
```tsx
class Demo extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      percent: 0,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(value: number) {
    this.setState({
      percent: value,
    })
  }
  
  render() {
    const {percent} = this.state;
    return (
      <div>
        <div>
          <Text>修改进度条百分比：</Text>
          <SpinBox onChange={this.handleChange} minValue={0} maxValue={100} defaultValue={percent} step={10} />
        </div>
        <Progress percent={percent} />
        <Progress type='circle' percent={percent} />
      </div>
    );
  }
}
```
:::

### 环形进度条改变初始角度
:::demo
```tsx
class Demo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDegree: 0,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(value: number) {
    this.setState({
      startDegree: value,
    })
  }
  
  render() {
    const {startDegree} = this.state;
    return (
      <div>
        <Progress type='circle' percent={30} startDegree={startDegree} />
        <div>
          <Text>修改环形进度条初始角度：</Text>
          <SpinBox onChange={this.handleChange} minValue={0} defaultValue={startDegree} step={10} />
        </div>
      </div>
    );
  }
}
```
:::

### 文案自定义进度条
:::demo
```tsx

const BASE_COLOR = '#00b7fe';
const COMPLETE_COLOR = '#43b52c';

class Demo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      percent: 100,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(value: number) {
    this.setState({
      percent: value,
    })
  }

  getLineProgressText(percent: number) {
    if(percent >= 100) {
      return <Icon type='check-circle' color={COMPLETE_COLOR} size={14} />;
    }
    return `${percent}%`;
  }

  getCircleProgressText(percent: number) {
    if(percent >= 100) {
      return <Icon type='round-check' color={COMPLETE_COLOR} size={20} />;
    }
    return `${percent}%`;
  }
  
  render() {
    const {percent} = this.state;
    return (
      <div>
        <div>
          <Text>修改进度条百分比：</Text>
          <SpinBox onChange={this.handleChange} minValue={0} maxValue={100} defaultValue={percent} step={10} />
        </div>
        <Progress percent={percent} showText={false} />
        <Progress percent={percent} format={value => `${value}分`} />
        <Progress percent={percent} strokeColor={percent >= 100 ? COMPLETE_COLOR : BASE_COLOR} format={this.getLineProgressText.bind(this)} />
        <Progress type='circle' percent={percent} showText={false} />
        <Progress type='circle' percent={percent} format={percent => `${(percent * 360) / 100}度`} />
        <Progress type='circle' strokeColor={percent >= 100 ? COMPLETE_COLOR : BASE_COLOR} percent={percent} format={this.getCircleProgressText.bind(this)} />
      </div>
    );
  }
}
```
:::

### 方角边缘进度条
:::demo
```tsx
class Demo extends React.Component {
  render() {
    return (
      <div>
        <Progress percent={30} strokeLinecap='square' />
        <Progress type='circle' percent={30} strokeLinecap='square' />
      </div>
    );
  }
}
```
:::

## Progress Props 描述

### 通用 Props 描述
| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| type | String | 'line' \| 'circle' | 进度条类型 |
| className | string | '' | 自定义className |
| style | React.CSSProperties | {} | 自定义style |
| percent | Number | 0 | 进度条的百分比|
| strokeColor | String | $primary-color | 进度条颜色 |
| trailColor | String | '#d9d9d9' | 进度条轨迹填充色 |
| strokeLinecap | 'round' \| 'square' | 'round' | 进度条边缘形状（圆角、方角） |
| format | (precent) => (React.Node \| string) | '' | 获取文案内容函数 |
| showText | Boolean | true | 是否显示进度文案内容 |

### type='line' props 描述
| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| strokeWidth | Number | 10 | 进度条宽度，单位是px |

### type='circle' props 描述
| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| width | Number | 100 | 视窗宽度 |
| height | Number | 100 | 视窗高度 |
| strokeWidth | Number | 6 | 进度条宽度，单位是进度条画布宽度的百分比 |
| startDegree | Number | 0 | 开始角度 |
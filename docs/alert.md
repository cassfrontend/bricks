## Alert
```tsx
import { Alert } from '@casstime/bricks';

<Alert message="Success Text" type="success" />
```

## 使用场景示例

### 1.普通使用
:::demo
```tsx

class Example extends React.Component {
  public render() {
    return (
      <div>
        <Alert message="成功提示（Success tips）" type="success" />
        <Alert message="信息注释（Informational Notes）" type="info"/>
        <Alert message="警告（Warning）" type="warning"/>
        <Alert message="错误提示（Error）" type="error"/>
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))

```
:::

### 2.带辅助性文字
:::demo
```tsx

class Example extends React.Component {
  public render() {
    return (
      <div>
        <Alert message="成功提示（Success tips）" description="辅助性文字介绍详细内容" type="success"/>
        <Alert message="信息注释（Informational Notes）" description="辅助性文字介绍详细内容" type="info"/>
        <Alert message="警告（Warning）" description="辅助性文字介绍详细内容" type="warning"/>
        <Alert message="错误提示（Error）" description="辅助性文字介绍详细内容" type="error"/>
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))

```
:::

### 3.带图标使用
:::demo
```tsx

class Example extends React.Component {
  public render() {
    return (
      <div>
        <Alert message="成功提示（Success tips）" showIcon type="success"/>
        <Alert message="信息注释（Informational Notes）" showIcon type="info"/>
        <Alert message="警告（Warning）" showIcon type="warning"/>
        <Alert message="错误提示（Error）" showIcon type="error"/>
        <Alert message="成功提示（Success tips）" description="辅助性文字介绍详细内容" showIcon type="success"/>
        <Alert message="信息注释（Informational Notes）" description="辅助性文字介绍详细内容" showIcon type="info"/>
        <Alert message="警告（Warning）" description="辅助性文字介绍详细内容" showIcon type="warning"/>
        <Alert message="错误提示（Error）" description="辅助性文字介绍详细内容" showIcon type="error"/>
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))

```
:::

### 4.左侧图标支持自定义
:::demo
```tsx

class Example extends React.Component {
  public render() {
    return (
      <div>
        <Alert message="成功提示（Success tips）" showIcon type="success" icon={<Icon type="car"/>}/>
        <Alert message="错误提示（Error）" description="辅助性文字介绍详细内容" showIcon type="error" icon={<Icon type="car"/>}/>
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))

```
:::

### 5.可关闭组件（扩展功能）
:::demo
```tsx

interface IAlertState {
  show: boolean;
  
}
class Example extends React.Component<{}, IAlertState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      show: true,
    };
  }
  public onClose() {
    this.setState({
      show: false,
    });
  }
  public render() {
    return (
      <div>     
        {this.state.show ?
         <Alert 
            message="成功提示（Success tips）"
            description="辅助性文字介绍详细内容" 
            type="success" 
            closable 
            onClose={this.onClose.bind(this)}/> 
        : null}
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))

```
:::

## props 描述

| 属性      | 类型                | 默认值    | 描述            |
| --------- | ------------------- | --------- | --------------- |
| message | ReactNode| -- | 标题文字信息 |
| description | ReactNode         | -- | 辅助性文字介绍 |
| showIcon | boolean              | false | 是否展示图标 |
| type|string(success，info，warning，error) | --| alert组件类型|
| closable | boolean         | false | 是否显示关闭按钮 |
| icon | ReactNode | -- | 左侧图标支持自定义 |
| onClose | function(() => void)  | false | 关闭按钮点击事件 |
| className | string              | undefined | 自定义className |
| style     | React.CSSProperties | undefined | 自定义style     |

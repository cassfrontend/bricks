# 步骤条

```tsx
import { Steps } from '@casstime/bricks';

<Steps current={0}>
  <Steps.Item title="step1" />
  <Steps.Item title="step2" />
  <Steps.Item title="step3" />
</Steps>
```

:::demo
```tsx
class Demo extends React.Component<{}, {}>{
  render() {
    return (
      <Steps current={1}>
        <Steps.Item title="已完成" />
        <Steps.Item title="进行中" />
        <Steps.Item title="未完成" />
      </Steps>
    );
  }
}
```
:::

### 综合示例

:::demo 所有节点未开始
```tsx
class Demo extends React.Component<{}, {}>{
  render() {
    return (
      <Steps>
        <Steps.Item title="步骤1" />
        <Steps.Item title="步骤2" />
        <Steps.Item title="步骤3" />
      </Steps>
    );
  }
}
```
:::

:::demo 使用自定义字符
```tsx
class Demo extends React.Component<{}, {}>{
  render() {
    return (
      <Steps current={1}>
        <Steps.Item title="已完成" char="A" />
        <Steps.Item title="进行中" char="B" />
        <Steps.Item title="未完成" char="C" />
      </Steps>
    );
  }
}
```
:::

:::demo 使用图标
```tsx
class Demo extends React.Component<{}, {}>{
  render() {
    return (
      <Steps current={1}>
        <Steps.Item title="已完成" />
        <Steps.Item title="进行中" iconClassName="icon-check" />
        <Steps.Item title="未完成" />
      </Steps>
    );
  }
}
```
:::

步骤条节点有三种状态，已完成，进行中，等待中
### Steps 组件

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| current | number | -1 | 进行中的节点index,在current之前的节点状态为已完成，之后的状态为等待中 |
| className | string | '' | 自定义className |

### Steps.Item 子组件

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| char | char | index + 1 | 节点的字符,可以为数字或英文字母，默认为1,2,3... |
| title | string | &nbsp; | 节点标题，要么都传，要么都不传 |
| className | string | undefined | 自定义className |
| iconClassName | string | &nbsp; | 图标的className，如： icon-check |

# Accordion

```tsx
import { Accordion } from '@casstime/bricks';

<Accordion onToggle={this.onAccordionPanelClick}>
  <Accordion.Panel title="1" active={activeIndex === 0}>
    Accordion-content1
  </Accordion.Panel>
  <Accordion.Panel title="2" active={activeIndex === 1}>
    Accordion-content2
  </Accordion.Panel>
</Accordion>
```

### 一般使用
:::demo 示例
``` tsx
interface IExampleState {
  activeIndex: number;
}
class Example extends React.Component<{}, IExampleState> {
    constructor(props: {}) {
      super(props);
      
      this.state = {
        activeIndex: -1,
      }

      this.onAccordionPanelClick = this.onAccordionPanelClick.bind(this);
    }

    onAccordionPanelClick(index: number) {
      console.log('index', index);
      const { activeIndex } = this.state;
      const newIndex = activeIndex === index ? -1 : index
      this.setState({
        activeIndex: newIndex,
      });
    }

     render() {
     const { activeIndex } = this.state;
        return (
            <div>
              <Accordion onToggle={this.onAccordionPanelClick}>
                <Accordion.Panel title="这是第一个Accordion.Panel" active={activeIndex === 0}>
                  <div>
                    这是展开的内容1
                  </div>
                </Accordion.Panel>
                <Accordion.Panel title="这是第二个Accordion.Panel" active={activeIndex === 1}>
                  <div>
                    这是展开的内容2
                  </div>
                </Accordion.Panel>
                <Accordion.Panel title="这是第三个Accordion.Panel" active={activeIndex === 2}>
                  <div>
                    这是展开的内容3
                  </div>
                </Accordion.Panel>
              </Accordion>
            </div>
        );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

### 可以展开多个面板
:::demo 示例
``` tsx
interface IExampleState {
  activeArray: boolean[];
}
class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      activeArray: [],
    }

    this.onAccordionPanelClick = this.onAccordionPanelClick.bind(this);
  }

  onAccordionPanelClick(index: number) {
    console.log('index', index);
    const activeArray = [...this.state.activeArray];
    activeArray[index] = !activeArray[index];
    this.setState({ activeArray });
  }

  render() {
    const { activeArray } = this.state;
    return (
      <div>
        <Accordion onToggle={this.onAccordionPanelClick}>
          <Accordion.Panel title="这是第一个Accordion.Panel" active={activeArray[0]}>
            <div>
              这是展开的内容1
                  </div>
          </Accordion.Panel>
          <Accordion.Panel title="这是第二个Accordion.Panel" active={activeArray[1]}>
            <div>
              这是展开的内容2
                  </div>
          </Accordion.Panel>
          <Accordion.Panel title="这是第三个Accordion.Panel" active={activeArray[2]}>
            <div>
              这是展开的内容3
                  </div>
          </Accordion.Panel>
        </Accordion>
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::


### 页面进入时激活某个面板
:::demo 示例
``` tsx
interface IExampleState {
  activeIndex: number;
}
class Example extends React.Component<{}, IExampleState> {
    constructor(props: {}) {
      super(props);
      
      this.state = {
        activeIndex: 0,
      }

      this.onAccordionPanelClick = this.onAccordionPanelClick.bind(this);
    }

    onAccordionPanelClick(index: number) {
       console.log('index', index);
       const { activeIndex } = this.state;
       const newIndex = activeIndex === index ? -1 : index
       this.setState({
         activeIndex: newIndex,
       })
    }

     render() {
     const { activeIndex } = this.state;
        return (
            <div>
              <Accordion onToggle={this.onAccordionPanelClick}>
                <Accordion.Panel title="这是第一个Accordion.Panel" active={activeIndex === 0}>
                  <div>
                    这是展开的内容1
                  </div>
                </Accordion.Panel>
                <Accordion.Panel title="这是第二个Accordion.Panel" active={activeIndex === 1}>
                  <div>
                    这是展开的内容2
                  </div>
                </Accordion.Panel>
                <Accordion.Panel title="这是第三个Accordion.Panel" active={activeIndex === 2}>
                  <div>
                    这是展开的内容3
                  </div>
                </Accordion.Panel>
              </Accordion>
            </div>
        );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

### 面板禁用展开或者收起
:::demo 示例
``` tsx
class Example extends React.Component {
  render() {
    return (
      <div>
        <Accordion>
          <Accordion.Panel title="这是一个Accordion.Panel" active={true} disabled={true}>
            <div>
              这是展开的内容
          </div>
          </Accordion.Panel>
        </Accordion>
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

## Accordion组件props描述
| 属性 | 类型 | 默认值| 描述 | 
| ---- | ----| ---- | ---- | 
| className | string | '' | Accordion样式 |
| style |  React.CSSProperties  | undefined | Accordion样式 |
| onToggle | function: (index:number) => void| undefined | 点击Accordion.Panel标题事件 |

## Accordion.Panel组件props描述
| 属性 | 类型 | 默认值| 描述 | 
| ---- | ----| ---- | ---- | 
| className | string | '' | Accordion.Panel样式 |
| style |  React.CSSProperties  | undefined | Accordion.Panel样式 |
| active | boolean | false | Accordion.Panel是否展开收起 |
| disabled | boolean | false | 是否阻止Accordion.Panel展开收起 |
| title | ReactNode | '' | Accordion.Panel标题信息 |
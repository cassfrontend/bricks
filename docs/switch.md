# switch组件

```tsx
import { Switch } from '@casstime/bricks';

<Switch onChange={this.onChange} defaultChecked={true} label="switch描述文字" textAlign="right"/>
```

### 一般使用(默认)
:::demo Switch组件示例

```tsx
class Demo extends React.Component<{}, {}> {
  constructor(props: {}){
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  onChange(checked: boolean){
    console.log('checked', checked);
  }

  render() {
    return <Switch
    onChange={this.onChange} 
    defaultChecked={true} 
    label="switch描述文字" 
    textAlign="right"/>
  }
}

```
:::

## 受控
:::demo Switch组件示例

```tsx
interface IDemoState{
  checked: boolean;
}
class Demo extends React.Component<{}, IDemoState> {
  constructor(props){
    super(props);
    this.state = {
      checked: false
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(checked: boolean){
    console.log('checked', checked);
    // setState之前不会变更状态
    //this.setState({ checked });
  }

  render() {
    return <Switch onChange={this.onChange} checked={this.state.checked}/>
  }
}

```
:::

## Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| className | string | undefined | 自定义className |
| style | React.CSSProperties | undefined | 自定义style |
| name | string | 'switch' | switch在表单中的name属性 |
| defaultChecked | boolean | false | 默认选中状态 |
| checked | boolean | undefined | 选中状态，如果传入checked，则组件受外部控制 |
| onChange | Function(checked) | null | 变化时回调函数 |
| disabled | boolean | undefined | 禁用switch开关 |
| label | string | undefined | switch外部描述文字 |
| textAlign | string | 'left' | switch外部描述文字左右位置，参数设置'left'或'right' |

# 树结构
```jsx
禁用状态:
子节点（disabled）--> 父节点（disabled）

半选状态点击事件:
响应：半选状态下点击父节点 --> 子节点全选
```

```tsx
import { Tree, ITree } from '@casstime/bricks';

<Tree treeData={treeData} onChange={this.handleChange} />
```

## 示例
:::demo
```jsx
const treeData = [
  {
    title: '0-0',
    key: '0-0',
    children: [{
      title: '0-0-0',
      key: '0-0-0',
      children: [
        { title: '0-0-0-0', key: '0-0-0-0' ,  disabled: true, checked: true},
        { title: '0-0-0-1', key: '0-0-0-1' , disabled: true},
        { title: '0-0-0-2', key: '0-0-0-2' },
      ],
    }, {
      title: '0-0-1',
      key: '0-0-1',
      children: [
        { title: '0-0-1-0', key: '0-0-1-0', checked: true },
        { title: '0-0-1-1', key: '0-0-1-1' },
        { title: '0-0-1-2', key: '0-0-1-2' },
      ],
    }, {
      title: '0-0-2',
      key: '0-0-2',
    }],
  },
  {
    title: '0-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-1',
    key: '0-1',
  }
];

class Demo extends React.Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
    this.state = { treeData };
  }

  handleChange(treeData) {
    this.setState({ treeData });
  }

  render() {
    return (
        <Tree treeData={this.state.treeData} onChange={this.handleChange}/>
    );
  }
}
```
:::


## 普通树组件 Tree

### props

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| treeData | ITree[] | -- | 树数据 |
| onChange | ITree[] | null | 选中或展开时触发 |

### ITree

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| key | string |  -- | (必填)key必须唯一 |
| title | string | -- | (必填)节点标题 |
| disabled | boolean | false | 节点是否为禁用状态|
| children | ITree[] |  -- | 子节点 |
| checked | boolean | -- | 选中状态 |
| expanded | boolean |  -- | 展开状态 |
| halfChecked | boolean | -- | 半选状态 |


## 高性能树组件 Tree.Virtualized

当树的数据比较多时，建议使用该组件。接口同Tree组件。
外层标签需要指定高度。

Tree.Virtualized 内部使用 react-virtualized 渲染。


:::demo 大量节点
```jsx

const defaultCount = 10000;

class Demo extends React.Component {
  constructor(props) {
    super(props);
    this.createTreeData = this.createTreeData.bind(this);
    this.handleSpinBoxChange = this.handleSpinBoxChange.bind(this);
    this.handleTreeChange = this.handleTreeChange.bind(this);
    this.clearChecked = this.clearChecked.bind(this);
    this.state = { treeData: this.createTreeData(defaultCount) }
  }

  handleSpinBoxChange(val) {
    this.setState({
      treeData: this.createTreeData(val)
    });
  }

  handleTreeChange(treeData) {
    this.setState({ treeData });
  }

  clearChecked() {
    this.setState({
      treeData: utils.clearTreeDataCheckedState(this.state.treeData)
    });
  }

  createTreeData(count) {
    return new Array(count)
      .fill(0)
      .map((data, i) => ({
        title: 'tree-' + i,
        key: 'key-' + i, 
        children: [{
          title: 'tree-sub-' + i, 
          key: 'key-sub' + i
        }] 
      }));
  }

  render() {
    return (
      <div>
        <div>
          <span>节点数量: </span>
          <SpinBox editable defaultValue={defaultCount} onChange={this.handleSpinBoxChange}/> 
          <Button outline onClick={this.clearChecked} className="pull-right">清空选中</Button>
        </div>
        <div style={{height: '300px', marginTop: '10px'}}>
          <Tree.Virtualized treeData={this.state.treeData} onChange={this.handleTreeChange}/>
        </div>
      </div>
    );
  }
}
```
:::

## utils

bricks内置了一些工具函数，用于对树进行操作

```jsx
import { utils } from '@casstime/bricks'

// 获取选中的叶子节点
utils.getTreeDataCheckedLeafs(treeData);

// 清空所有选中的节点，参考上例,返回新的treeData
utils.clearTreeDataCheckedState(treeData);

```

<!-- 
## TreeNode

:::demo
```jsx
const treeNode = {
  title: '0-0',
  key: '0-0',
  children: [{
    title: '0-0-0',
    key: '0-0-0',
    children: [
      { title: '0-0-0-0', key: '0-0-0-0' },
      { title: '0-0-0-1', key: '0-0-0-1' },
      { title: '0-0-0-2', key: '0-0-0-2' },
    ],
  }, {
    title: '0-0-1',
    key: '0-0-1',
    children: [
      { title: '0-0-1-0', key: '0-0-1-0' },
      { title: '0-0-1-1', key: '0-0-1-1' },
      { title: '0-0-1-2', key: '0-0-1-2' },
    ],
  }, {
    title: '0-0-2',
    key: '0-0-2',
  }],
};

class Demo extends React.Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
    this.state = { treeNode };
  }

  handleChange(treeNode) {
    this.setState({ treeNode });
  }

  render() {
    return (
      <ul>
        <VirtualizedTree.TreeNode treeNode={this.state.treeNode} onChange={this.handleChange}/>
      </ul>
    );
  }
}
```
:::
-->

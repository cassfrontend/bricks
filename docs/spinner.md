# spinner组件

```tsx
import { Spinner } from '@casstime/bricks';

<Spinner />
```

:::demo

```tsx
class Demo extends React.Component<{} ,{}> {
  render() {
    return (
      <Spinner />
    )
  }
}
```
:::

## 参数描述
| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| className | string | '' | 自定义 className |
| style | React.CSSProperties | undefined | 自定义样式 |
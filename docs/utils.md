# 工具class

bricks中内置了一些辅助class

- `pull-right` 元素向右浮动
- `pull-left` 元素向左浮动
- `clearfix` 清除浮动
- `hidden` 隐藏元素
- `align-left` text-align: left
- `align-right` text-align: right
- `align-center` text-align: center
- `no-padding` 清除padding
- `no-border` 清除border
- `no-margin` 清除margin
- `no-border-radius` 清除圆角
- `br-scroll` 封装之后的滚动条样式
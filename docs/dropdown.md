# 下拉组件 Dropdown

```tsx
import { Dropdown } from '@casstime/bricks';
<Dropdown open={true} 
  controller={<Button>controller</Button>}
  >
  {() => <div>content</div>}
</Dropdown>
```

### 一般使用

:::demo 示例

```tsx
interface IExampleState {
  open: boolean;
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      open: false,
    }
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({
      open: !this.state.open,
    });
  }

  render() {
    return (
      <div>
        <Dropdown open={this.state.open} controller={<Button onClick={this.handleClick}>下拉组件</Button>}>
          {() => <div style={{height: 100, width: 150, background: '#999'}}>dropdown</div>}
        </Dropdown>
      </div>
    );
  }
}
ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))

```
:::

## Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| open | boolean | false | 是否下拉 |
| dropdownKey | string | - | 当内容高度不固定时，需要变更key，这样才能更新下拉框 |
| controller | Element | - | 控制组件，弹层根据controller定位 |
| children | ComponentType | - | 下拉弹层内容 |


## Input controller


:::demo 示例
```tsx
interface IExampleState {
  open: boolean;
  value: string;
  dropdownKey?: string;
  key?: number;
  options: string[];
}
class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      open: false,
      dropdownKey: '',
      options: [],
      value: ''
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.openDropdown = this.openDropdown.bind(this);
    this.closeDropdown = this.closeDropdown.bind(this);
  }

  filter(keywords: string) {
    const all =  ['橘子', '苹果', '葡萄', '香蕉', '香梨', '石榴'];
    return all.filter(item => item.includes(keywords));
  }

  handleClick(option: string) {
    this.setState({
      open: false,
      value: option
    });
  }

  handleChange(event: React.ChangeEvent<HTMLInputElement>) {
    const options = this.filter(event.target.value);
    console.log(event.target.value, options);
    this.setState({
      value: event.target.value,
      options,
      key: options.length,
    });
  }

  openDropdown() {
    const options = this.filter(this.state.value)
    this.setState({
      open: true,
      key: options.length,
      options
    });
  }

  closeDropdown() {
    this.setState({ open: false });
  }

  render() {
    console.log(this.state)
    return (
      <div>
        <Dropdown
        dropdownKey={this.state.key}
          open={this.state.open}
          onDismiss={this.closeDropdown}
          controller={<Input
            value={this.state.value}
            onFocus={this.openDropdown}
            onChange={this.handleChange}
          />}>
          {() => (
            !!this.state.options.length && <ul style={{ marginTop: 10, border: '1px solid #ddd', background: '#fff', padding: 10 }}>
              { this.state.options.map((option) =>
                <li style={{height: 20}} onClick={() => this.handleClick(option)}>
                  {option}
                </li>)
              }
            </ul>
          )}
        </Dropdown>
      </div>
    );
  }
}
ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))

```
:::
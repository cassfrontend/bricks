# Modal

```tsx
import { Modal } from '@casstime/bricks';
```

### 一般使用

:::demo
```tsx
interface IDemoState {
  visible: boolean;
}
class Demo extends React.Component<{}, IDemoState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      visible: false,
      value: ''
    }
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.handleChange = this.handleChange.bind(this)
  }

  showModal() {
    this.setState({ visible: true });
  }

  handleChange(e) {
    this.setState({ value: e.target.value })
  }
  
  hideModal() {
    this.setState({ visible: false });
  }

  render() {
    return (
      <div>
        <Button onClick={this.showModal}>Open</Button>
        <Modal showBackdrop visible={this.state.visible} onBackdropClick={this.hideModal}>
          <Modal.Header onClose={this.hideModal}>
            <Text bold>标题</Text>
          </Modal.Header>
          <Modal.Content>
            <FormGroup success>
              <Col sm={3}>
                <FormLabel align="right">姓名：</FormLabel>
              </Col>
              <Col sm={6}>
                <FormControl><Input /></FormControl>
              </Col>
            </FormGroup>
            <FormGroup success>
              <Col sm={3}>
                <FormLabel align="right">邮箱：</FormLabel>
              </Col>
              <Col sm={6}>
                <FormControl><Input value={this.state.value} onChange={this.handleChange}/></FormControl>
              </Col>
            </FormGroup>
          </Modal.Content>
          <Modal.Footer>
            <Button onClick={this.hideModal}>确定</Button>
            <Button onClick={this.hideModal} outline>取消</Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}

```
:::

## Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
|  visible      | boolean | false   | 是否显示弹窗   |
|  showBackdrop | boolean |undefined| 是否显示遮罩层 | 
|onBackdropClick| Function| &nbsp;  | 点击遮罩层回调 | 
|  className    | string  | ''      | 自定义类名     |
|  style        | object  |{background:red....}| 自定义样式     |

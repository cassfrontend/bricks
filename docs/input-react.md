# Input输入框
 
## 一般使用
```tsx
import { Input } from '@casstime/bricks';

<Input value={this.state.value} onChange={this.handleChange} />
```

:::demo 示例

```tsx
interface IDemoState{
  value?: string;
}

class Demo extends React.Component<{}, IDemoState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      value: 'circle'
    }
    this.onChange = this.onChange.bind(this);
  }
  onChange(e) {
    console.log('输入值变化', e.target.value)
    this.setState({
      value:  e.target.value
    })
  }

  render() {
    return (
      <div>
        <Input
          value={this.state.value}
          onChange={this.onChange}
          placeholder="请输入..."
        />
      </div>
    );
  }
}
// PLAYER_ID是注入的挂载点
ReactDOM.render(<Demo />, document.getElementById(PLAYER_ID))

```
:::


## 数值类型的输入框
```tsx
import { Input } from '@casstime/bricks';

<Input number value={this.state.value} onChange={this.handleChange} />
```



:::demo 示例

```tsx

interface IDemoState{
  value?: string;
}

class Demo extends React.Component<{}, IDemoState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      value: '',
    }
    this.onChange = this.onChange.bind(this);
  }
  onChange(e) {
    console.log('输入值变化', e.target.value)
    this.setState({
      value:  e.target.value
    })
  }

  render() {
    return (
      <div>
        <Input
          number
          value={this.state.value}
          onChange={this.onChange}
          placeholder="请输入数值..."
        />
      </div>
    );
  }
}
// PLAYER_ID是注入的挂载点
ReactDOM.render(<Demo />, document.getElementById(PLAYER_ID))

```
:::


## 带图标的输入框

```tsx
<Input icon={<Icon type="search" />} />
```

:::demo 示例

```tsx
interface IDemoState{
  value?: string;
}

class Demo extends React.Component<{}, IDemoState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      value: 'circle'
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    console.log('输入值变化', e.target.value)
    this.setState({
      value:  e.target.value
    })
  }

  render() {
    return (
      <div>
        <Input
          icon={<Icon type="search" />}
          value={this.state.value}
          onChange={this.onChange}
          placeholder="请输入..."
        />
      </div>
    );
  }
}
// PLAYER_ID是注入的挂载点
ReactDOM.render(<Demo />, document.getElementById(PLAYER_ID))

```
:::

## error状态

```tsx
<Input error />
```

:::demo 示例

```tsx
interface IDemoState{
  value?: string;
}

class Demo extends React.Component<{}, IDemoState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      value: 'circle'
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    console.log('输入值变化', e.target.value)
    this.setState({
      value:  e.target.value
    })
  }

  render() {
    return (
      <div>
        <Input
          error
          icon={<Icon type="search" />}
          value={this.state.value}
          onChange={this.onChange}
          placeholder="请输入..."
        />
      </div>
    );
  }
}
// PLAYER_ID是注入的挂载点
ReactDOM.render(<Demo />, document.getElementById(PLAYER_ID))

```
:::


## disabled 状态

```tsx
<Input disabled />
```

:::demo 示例

```tsx
interface IDemoState{
  value?: string;
}

class Demo extends React.Component<{}, IDemoState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      value: 'circle'
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    console.log('输入值变化', e.target.value)
    this.setState({
      value:  e.target.value
    })
  }

  render() {
    return (
      <div>
        <Input
          disabled
          icon={<Icon type="search" />}
          value={this.state.value}
          onChange={this.onChange}
          placeholder="请输入..."
        />
      </div>
    );
  }
}
// PLAYER_ID是注入的挂载点
ReactDOM.render(<Demo />, document.getElementById(PLAYER_ID))

```
:::

## 不同尺寸

Input组件除了默认大小外，还支持 xSmall,small,large,xLarge等不同尺寸

```tsx
<Input small />
```

:::demo 示例

```tsx
class Example extends React.Component<{}, {}>{
  render() {
    return (
      <div>
        <Input value="xSmall" xSmall disabled/>
        <br/>
        <Input value="small" small disabled/>
        <br/>
        <Input value="default" disabled/>
        <br/>
        <Input value="large" large disabled/>
        <br/>
        <Input value="xLarge" xLarge disabled/>
      </div>
    );
  }
}
ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

## 属性列表

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| disabled | boolean | false | 是否禁用状态 |
| error | boolean | false | 组件是否出错 |
| xSmall   | boolean | false | 改变组件大小（XS) |
| small    | boolean | false | 改变组件大小（S) |
| large    | boolean | false | 改变组件大小（L) |
| xLarge   | boolean | false | 改变组件大小（XL) |
| type     | string  | text | 同原生的input标签 |
| defaultValue | string | '' | 输入框初始内容 |
| value    | string | - | 输入框的内容(使用value需要有onChange方法) |
| number   | boolean | false | 输入框只支持数值类型 |
| maxLength | number | - | 输入框的内容字符长度 |
| icon | Element | '' | 输入框的结尾的图标``` icon={<Icon type="search"/>} ``` |
| onFocus | Function(event) | - | 同原生input |
| onBlur | Function(event) | - | 同原生input |
| onKeyUp | Function(event) | - | 同原生input |
| onKeyDown | Function(event) | - | 同原生input |
| onChange | Function(event) | - | 同原生input |
| onClick | Function(event) | - | 点击框内(后)事件 |


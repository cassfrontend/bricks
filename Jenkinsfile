pipeline {
  agent {
    node { label 'master' }
  }

  environment {
    DINGDING_ROBOT_URL = "https://oapi.dingtalk.com/robot/send?access_token=${DINGDING_TOKEN}"
    MASTER_WORKSPACE = "${WORKSPACE}"
  }

  stages {
    stage('拉取文档') {
      steps {
        dir('site/build') {
          withCredentials([usernamePassword(credentialsId: "${GIT_CREDENTIALS_ID}", passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
            git url: "https://${GIT_USERNAME}:${GIT_PASSWORD}@gitee.com/cassfrontend/bricks-docs.git"
            sh 'ls'
          }
        }
      }
    }

    stage('在docker构建') {
      agent {
        docker {
          image 'node:10-alpine'
          args '-v $MASTER_WORKSPACE:$MASTER_WORKSPACE'
        }
      }
      stages {
        stage('初始化') {
          steps {
            sh 'SASS_BINARY_SITE=https://npm.taobao.org/mirrors/node-sass/ npm install --registry=https://registry.npm.taobao.org'
            script {
              env.BRICKS_VERSION = sh(returnStdout: true, script: 'node -pe "require(\'./package.json\').version"')
            }
          }
        }

        stage('单元测试') {
          steps {
            sh 'npm test'
          }
        }

        stage('构建发布npm包') {
          steps {
            sh "echo '//registry.npmjs.org/:_authToken=${NPM_TOKEN}' > ~/.npmrc"
            sh "npm run build"
            sh "cp -rf site/build/* ${MASTER_WORKSPACE}/site/build"
            sh "cp site/index.html ${MASTER_WORKSPACE}/site/build/v${BRICKS_VERSION}"
            sh "npm publish --access public"
          }
        }
      }
    }

    stage('发布文档') {
      steps {
        dir('site/build') {
          sh("git add .")
          sh("git commit -am 'auto commit'")
          sh('git push origin master')
        }
      }
    }
  }

  post {
    success {
      script {
        def rawmsg = successNotifyData()
        sh "curl -H 'Content-Type:application/json' -X POST --data '${rawmsg}' ${env.DINGDING_ROBOT_URL}"
      }
    }

    failure {
      script {
        def rawmsg = failNotifyData()
        sh "curl -H 'Content-Type:application/json' -X POST --data '${rawmsg}' ${env.DINGDING_ROBOT_URL}"
      }
    }
  }
}


def failNotifyData() {
  def changes = getChangeListByBuild(currentBuild)
  def title = "${env.JOB_NAME} 构建失败[${env.BUILD_NUMBER}]"
  def markdown = "### ${title}\n #### 新增特性 ${changes} \n #### 摘要 > buildUrl: ${env.BUILD_URL} \n > [点击查看](${env.BUILD_URL}/console)"
  return buildJSON(title, markdown)
}

def successNotifyData() {
  def changes = getChangeList()
  def url = "${env.BUILD_URL}/artifact/artifacts/";
  def title = "${env.JOB_NAME} 构建成功 [${env.BUILD_NUMBER}]"
  def markdown = """
### ${title}
#### 新增特性:
${changes}
#### 构建结果:
> 发布版本: @casstime/bricks@${BRICKS_VERSION}
> 文档链接: https://cassfrontend.gitee.io/bricks-docs/v${BRICKS_VERSION}
"""
  return buildJSON(title, markdown)
}

def buildJSON(title, markdown) {
  return "{\"msgtype\":\"markdown\",\"markdown\":{\"title\":\"${title}\",\"text\":\"${markdown}\"}}"
}

def getChangeList() {
  def text = ""
  def build = currentBuild
  text += getChangeListByBuild(build)
  build = build.previousBuild
  while (build && build.resultIsWorseOrEqualTo("UNSTABLE")) {
    text += getChangeListByBuild(build)
    build = build.previousBuild
  }
  return text
}

def getChangeListByBuild(build) {
  def text = ""
  for (changeSetList in build.changeSets) {
    for (changeSet in changeSetList) {
      text += "- ${changeSet.author.fullName} [${changeSet.msg}](https://gitee.com/cassfrontend/bricks/commit/${changeSet.commitId})\n"
    }
  }
  return text
}

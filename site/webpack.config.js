const webpack = require('webpack');
const pkg = require('../package.json');

module.exports = {
  mode: "development",
  entry: './index.js',
  output: {
    path: __dirname + `/build/v${pkg.version}`,
    filename: "bundle.js"
  },
  resolve: {
    extensions: [".js", ".jsx", '.ts', '.tsx']
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      use: [
        {
          loader: 'babel-loader',
          options: {
            "babelrc": false,
            "presets": [
              [
                "@babel/preset-env",
                {
                  "targets": "ie > 8"
                }
              ],
              "@babel/preset-react"
            ],
            "plugins": [
              "@babel/plugin-syntax-dynamic-import"
            ]
          }
        },
      ]
    },
    {
      test: /\.tsx?$/,
      use: [
        {
          loader: 'babel-loader',
          // options: {
          //   "babelrc": false,
          //   "presets": [
          //     [
          //       "env",
          //       {
          //         "targets": "not ie <= 8"
          //       }
          //     ],
          //     "react",
          //     "stage-2"
          //   ],
          //   "plugins": [
          //     "syntax-dynamic-import"
          //   ]
          // }
        },
        "ts-loader"
      ]
    },
    {
      test: /\.css$/,
      loader: "style-loader!css-loader"
    },
    {
      test: /\.scss$/,
      loader: 'style-loader!css-loader!sass-loader'
    },
    {
      test: /\.less$/,
      loader: 'style-loader!css-loader!less-loader'
    },
    {
      test: /\.(eot|svg|ttf|woff|woff2|gif|png|jpg|jpeg)(\?.+)?$/,
      loader: 'file-loader'
    },
    {
      test: /\.md$/,
      loader: 'raw-loader'
    }]
  }
}

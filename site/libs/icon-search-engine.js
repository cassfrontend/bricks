export default function iconTemp(iconList) {
  return `
    constructor(props) {
      super(props);
      this.state = {
        iconList: ${iconList},
        filterList: ${iconList}
      }
    }
    render() {
      return (
        <div>
          <div className="icon-search-panel">
            <input
              type="text"
              placeholder="输入关键词搜索，比如 circle"
              onChange={e => {
                if (!e.target.value) {
                  this.setState({filterList: this.state.iconList});
                  return;
                }
                let newIconList = {};
                Object.keys(this.state.iconList).forEach(key => {
                  newIconList[key] = [];
                  this.state.iconList[key].forEach(item => {
                    if (item.iconName.includes(e.target.value) || item.tag.includes(e.target.value)) {
                      newIconList[key].push(item);
                    }
                  })
                })
                this.setState({filterList: newIconList})
              }}
            />
            <div className="title-tip">点击下面的图标按钮可以直接复制图标名称(type)</div>
          </div>
          {Object.keys(this.state.filterList).map((item, k) =>
            this.state.filterList[item].length > 0 && <div className="icon-table" key={k}>
              <h4 className="icon-table-title">{item}</h4>
              <ul>
                {this.state.filterList[item].map((v, i) =>
                  <li key={i} onClick={e => {
                    const inp =document.createElement('input');
                    document.body.appendChild(inp)
                    inp.value = v.iconName.substring(5);
                    inp.select();
                    document.execCommand('copy',false);
                    inp.remove();
                    const target = e.currentTarget.querySelector('.copy-tip');
                    target.classList.add('copy-tip-show');
                    setTimeout(function(){
                      target.classList.remove('copy-tip-show')
                    }, 1000);
                  }}>
                    <span className="copy-tip">复制成功</span>
                    <i className={\`icon \${v.iconName}\`} />
                    <span className="icon-name">{v.iconName}</span>
                  </li>
                )}
              </ul>
            </div>
          )}
        </div>
      )
    }
  `;
}


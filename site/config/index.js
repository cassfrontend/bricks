const componentClassify = {
  GENERAL: 'GENERAL',
  NAVIGATOR: 'NAVIGATOR',
  DATA_ENTRY: 'DATA_ENTRY',
  DATA_DISPLAY: 'DATA_DISPLAY',
  FEEDBACK: 'FEEDBACK',
  OTHERS: 'OTHERS',
}

const pages = {
  'README': {
    'readme': {
      title: '使用说明',
      document: require('../../docs/readme.md'),
    },
    'developer': {
      title: '开发者文档',
      document: require('../../docs/developer.md'),
    },
    'migration': {
      title: '迁移说明(升级至V0.21.0)',
      document: require('../../docs/migration.md'),
    },
    'changelog': {
      title: '更新日志',
      document: require('../../CHANGELOG.md'),
    }
  },
  'CSS': {
    'a-Utils': {
      title: '工具class',
      document: require('../../docs/utils.md'),
    },
  },
  'REACT': {
    'tooltip': {
      title: 'Tooltip-文字提示',
      type: componentClassify.FEEDBACK,
      document: require('../../docs/tooltip.md'),
    },
    'icons': {
      title: 'Icon-图标',
      type: componentClassify.GENERAL,
      document: require('../../docs/icons.md'),
    },
    'text': {
      title: 'Text-文字',
      type: componentClassify.GENERAL,
      document: require('../../docs/text.md'),
    },
    'form': {
      title: 'Form-表单',
      type: componentClassify.DATA_ENTRY,
      document: require('../../docs/form.md'),
    },
    'grid': {
      title: 'Grid-栅格布局',
      type: componentClassify.GENERAL,
      document: require('../../docs/grid.md'),
    },
    'button-react': {
      title: 'Button-按钮',
      type: componentClassify.GENERAL,
      document: require('../../docs/button-react.md'),
    },
    'menu-react': {
      title: 'Menu-菜单',
      type: componentClassify.NAVIGATOR,
      document: require('../../docs/menu-react.md'),
    },
    'tabs': {
      title: 'Tabs-页签',
      type: componentClassify.NAVIGATOR,
      document: require('../../docs/tabs.md'),
    },
    'bar-react': {
      title: 'Bar-横条组件',
      type: componentClassify.NAVIGATOR,
      document: require('../../docs/bar-react.md'),
    },
    'collapse': {
      title: 'Collapse-折叠组件',
      type: componentClassify.DATA_DISPLAY,
      document: require('../../docs/collapse.md'),
    },
    'accordion': {
      title: 'Accordion-手风琴',
      type: componentClassify.DATA_DISPLAY,
      document: require('../../docs/accordion.md'),
    },
    'fade': {
      title: 'Fade-淡入淡出组件',
      type: componentClassify.OTHERS,
      document: require('../../docs/fade.md'),
    },
    'date-range-picker': {
      title: 'DateRangePicker-日期范围组件',
      type: componentClassify.DATA_ENTRY,
      document: require('../../docs/date-range-picker.md'),
    },
    'panel': {
      title: 'Panel-面板组件',
      type: componentClassify.OTHERS,
      document: require('../../docs/panel.md'),
    },
    'checkbox': {
      title: 'Checkbox-复选框',
      type: componentClassify.DATA_ENTRY,
      document: require('../../docs/checkbox.md'),
    },
    'dropdown': {
      title: 'Dropdown-下拉菜单',
      type: componentClassify.DATA_DISPLAY,
      document: require('../../docs/dropdown.md'),
    },
    'select': {
      title: 'Select-下拉选择框',
      type: componentClassify.DATA_ENTRY,
      document: require('../../docs/select.md'),
    },
    'multiselect': {
      title: 'Multiselect-复选下拉选择框',
      type: componentClassify.DATA_ENTRY,
      document: require('../../docs/multiselect.md'),
    },
    'ganged-select': {
      title: 'GangedSelect-级联选择',
      type: componentClassify.DATA_ENTRY,
      document: require('../../docs/ganged-select.md'),
    },
    'tree': {
      title: 'Tree-树结构',
      type: componentClassify.DATA_DISPLAY,
      document: require('../../docs/tree.md'),
    },
    'transfer': {
      title: 'Transfer-穿梭框',
      type: componentClassify.DATA_ENTRY,
      document: require('../../docs/transfer.md'),
    },
    'pagination': {
      title: 'Pagination-分页组件',
      type: componentClassify.NAVIGATOR,
      document: require('../../docs/pagination.md'),
    },
    'dialog': {
      title: 'Dialog-对话框组件',
      type: componentClassify.FEEDBACK,
      document: require('../../docs/dialog.md'),
    },
    'date-picker': {
      title: 'DatePicker-日历组件',
      type: componentClassify.DATA_ENTRY,
      document: require('../../docs/date-picker.md'),
    },
    'table': {
      title: 'Table-表格组件',
      type: componentClassify.DATA_DISPLAY,
      document: require('../../docs/table.md'),
    },
    'input-react': {
      title: 'Input-输入框组件',
      type: componentClassify.DATA_ENTRY,
      document: require('../../docs/input-react.md'),
    },
    'radio': {
      title: 'Radio-单选按钮',
      type: componentClassify.DATA_ENTRY,
      document: require('../../docs/radio.md'),
    },
    'spin-box': {
      title: 'SpinBox-数字调整框',
      type: componentClassify.DATA_ENTRY,
      document: require('../../docs/spin-box.md'),
    },
    'time-line': {
      title: 'TimeLine-时间轴',
      type: componentClassify.DATA_DISPLAY,
      document: require('../../docs/time-line.md'),
    },
    'progress': {
      title: 'Progress-进度条',
      type: componentClassify.DATA_DISPLAY,
      document: require('../../docs/progress.md'),
    },
    'tag': {
      title: 'Tag-标签',
      type: componentClassify.DATA_DISPLAY,
      document: require('../../docs/tag.md'),
    },
    'breadcrumb-react': {
      title: 'Breadcrumb-面包屑',
      type: componentClassify.NAVIGATOR,
      document: require('../../docs/breadcrumb-react.md'),
    },
    // 'navbar': {
    //   title: 'Navbar 工具栏',
    //  type: componentClassify.NAVIGATOR,
    //   document: require('../../docs/navbar.md'),
    // },
    'steps': {
      title: 'Steps-步骤条',
      type: componentClassify.NAVIGATOR,
      document: require('../../docs/steps.md'),
    },
    'backdrop': {
      title: 'Backdrop-背景幕',
      type: componentClassify.OTHERS,
      document: require('../../docs/backdrop.md'),
    },
    'confirm': {
      title: 'Confirm-弹窗',
      type: componentClassify.FEEDBACK,
      document: require('../../docs/confirm.md'),
    },
    'toast': {
      title: 'Toast-弹窗',
      type: componentClassify.FEEDBACK,
      document: require('../../docs/toast.md'),
    },
    'modal': {
      title: 'Modal-通用弹窗',
      type: componentClassify.FEEDBACK,
      document: require('../../docs/modal.md'),
    },
    'spinner': {
      title: 'Spinner-加载图标',
      type: componentClassify.FEEDBACK,
      document: require('../../docs/spinner.md'),
    },
    'switch': {
      title: 'Switch-开关组件',
      type: componentClassify.DATA_ENTRY,
      document: require('../../docs/switch.md'),
    },
    'carousel': {
      title: 'Carousel-走马灯',
      type: componentClassify.DATA_DISPLAY,
      document: require('../../docs/carousel.md')
    },
    'popover': {
      title: 'Popover-气泡组件',
      type: componentClassify.DATA_DISPLAY,
      document: require('../../docs/popover.md'),
    },
    'cascader': {
      title: 'Cascader-级联组件',
      type: componentClassify.DATA_ENTRY,
      document: require('../../docs/cascader.md')
    },
    'alert': {
      title: 'Alert-提示组件',
      type: componentClassify.FEEDBACK,
      document: require('../../docs/alert.md')
    },
    'chain-select': {
      title: 'ChainSelect-级联组件',
      type: componentClassify.DATA_ENTRY,
      document: require('../../docs/chain-select.md')
    },
  },
}

const classify2CN = {
  GENERAL: '通用',
  NAVIGATOR: '导航',
  DATA_ENTRY: '数据录入',
  DATA_DISPLAY: '数据展示',
  FEEDBACK: '反馈',
  OTHERS: '其他',
}

module.exports = {
  pages,
  componentClassify,
  classify2CN,
}

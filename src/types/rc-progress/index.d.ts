import React from 'react';

export interface IRcProgressProps {
  strokeWidth?: number; //default 1.	Width of the stroke. Unit is percentage of SVG canvas size.
  strokeColor?: string; //default #2db7f5.	Stroke color.
  trailWidth?: number; //default 1.	Width of the trail stroke. Unit is percentage of SVG canvas size. Trail is always centered relative to actual progress path. If trailWidth are not defined, it same as strokeWidth.
  trailColor?: string; //default #D9D9D9.	Color for lighter trail stroke underneath the actual progress path.
  strokeLinecap?: string; //default 'round'.	The shape to be used at the end of the progress bar, can be `butt`, `square` or `round`.
  prefixCls?: string; //default rc-progress.	prefix className for component
  className?: string; //customized className
  style?: object; //style object will be added to svg element
  percent?: number; //default 0.	the percent of the progress
  gapDegree?: number; //default 0.	the gap degree of half circle, 0 - 360
  gapPosition?: string; //default top.	the gap position, value: top, bottom, left, right.
}
export class Line extends React.Component<IRcProgressProps> {

}

export class Circle extends React.Component<IRcProgressProps> {

}

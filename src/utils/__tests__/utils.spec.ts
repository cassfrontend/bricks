import { getTreeDataDepth, getTreeDataCheckedNodes } from '../utility';

describe('utils', () => {
  describe('getTreeDataDepth', () => {
    it('空数组', () => {
      expect(getTreeDataDepth([])).toEqual(0);
    });

    it('一层数据', () => {
      expect(getTreeDataDepth([{ key: '1', title: '1' }, { key: '2', title: '2', children: [] }])).toEqual(1);
    });

    it('两层数据', () => {
      const treeData = [
        { key: '1', title: '1' },
        {
          key: '2', title: '2', children: [
            { key: '2-1', title: '2-1' },
          ],
        }];
      expect(getTreeDataDepth(treeData)).toEqual(2);
    });

    it('多层数据', () => {
      const treeData = [
        { key: '1', title: '1' },
        {
          key: '2', title: '2', children: [
            {
              key: '2-1', title: '2-1', children: [
                {
                  key: '2-1-1', title: '2-1-1', children: [
                    { key: '2-1-1-1', title: '2-1-1-1' },
                  ],
                },
              ],
            },
          ],
        }];
      expect(getTreeDataDepth(treeData)).toEqual(4);
    });
  });

  describe('getTreeDataCheckedNodes返回选中的最高一级treeNode的集合', () => {
    it('两层数据', () => {
      const treeData = [
        { key: '1', title: '1', checked: true },
        {
          key: '2', title: '2', checked: true, children: [
            {
              key: '2-1', title: '2-1', children: [
                {
                  key: '2-1-1', title: '2-1-1', children: [
                    { key: '2-1-1-1', title: '2-1-1-1' },
                  ],
                },
              ],
            },
          ],
        }];
      const nodes = getTreeDataCheckedNodes(treeData, 'LATEST_NODE');
      expect(nodes.length).toEqual(2);
      expect(nodes[0].key).toEqual('1');
      expect(nodes[1].key).toEqual('2');
    });
  });
});

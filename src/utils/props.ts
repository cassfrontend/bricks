import { Omit } from './utility';

/** 原生dom事件的interface */
export type IDOMProps = React.DOMAttributes<HTMLElement>;

/** 原生dom事件的去掉onChange、onClick等的interface */
export type IControledDOMProps = Omit<IDOMProps, 'onChange' | 'onClick' | 'onSelect'>;

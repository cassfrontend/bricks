import { shallow } from 'enzyme';
import React from 'react';
import withValidator, { IValidatorProps } from '../withValidator';

describe('withValidator', () => {
  it('Regex规则', () => {
    interface IFormProps extends IValidatorProps {
      a: string;
    }

    class Form extends React.Component<IFormProps, { value: string }> {
      constructor(props: IFormProps) {
        super(props);
        this.props.setValues(() => ({
          value: this.state.value,
        }));
        this.changeValue = this.changeValue.bind(this);
      }
      public changeValue(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState({ value: event.target.value });
      }
      public render() {
        return <input value={this.state.value} onChange={this.changeValue} />;
      }
    }

    const WrapperForm = withValidator({ value: /\S+/ })(Form);
    const wrapper = shallow(<WrapperForm a="12" />);
    expect(wrapper.find(Form).props().a).toEqual('12');
  });
});

import { shallow } from 'enzyme';
import React from 'react';
import { Grid } from '../Grid';

describe('Grid', () => {
  it('<Grid /> 渲染成div', () => {
    const wrapper = shallow(<Grid />);
    expect(wrapper.type()).toEqual('div');
  });

  it('<Grid /> 默认含有class container', () => {
    const wrapper = shallow(<Grid />);
    expect(wrapper.hasClass('container')).toEqual(true);
  });

  it('<Grid fluid /> 含有class container-fluid', () => {
    const wrapper = shallow(<Grid fluid />);
    expect(wrapper.hasClass('container-fluid')).toEqual(true);
  });

  it('<Grid /> 支持自定义className', () => {
    const wrapper = shallow(<Grid className="test test1" />);
    expect(wrapper.hasClass('test')).toEqual(true);
    expect(wrapper.hasClass('test1')).toEqual(true);
  });
});

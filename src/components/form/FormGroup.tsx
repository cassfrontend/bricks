import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import { IDOMProps } from '../../utils/props';

import { IFormControlProps } from './FormControl';

export interface IFormGroupProps extends IFormControlProps, IDOMProps {
  xSmall?: boolean;
  small?: boolean;
  large?: boolean;
  xLarge?: boolean;
}

export const FormGroup: React.SFC<IFormGroupProps> = ({
  children,
  className, style,
  error, warn, success,
  xSmall, small, large, xLarge,
  ...domProps
}: IFormGroupProps) => {
  const classes = {
    'row': true,
    'br-form-group': true,

    'br-form-group--error': error,
    'br-form-group--warn': warn,
    'br-form-group--success': success,

    'br-form-group--xs': xSmall,
    'br-form-group--sm': small,
    'br-form-group--lg': large,
    'br-form-group--xl': xLarge,
  };

  return (
    <div
      {...domProps as IDOMProps}
      className={classNames(classes, className)}
      style={style}
    >
      {children}
    </div>
  );
};

FormGroup.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  style: PropTypes.object,
  error: PropTypes.bool,
  warn: PropTypes.bool,
  success: PropTypes.bool,

  xSmall: PropTypes.bool,
  small: PropTypes.bool,
  large: PropTypes.bool,
  xLarge: PropTypes.bool,
};

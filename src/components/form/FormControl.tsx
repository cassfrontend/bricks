import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import { IDOMProps } from '../../utils/props';

export interface IFormControlProps extends IDOMProps {
  children?: React.ReactNode;
  className?: string;
  style?: React.CSSProperties;
  error?: boolean;
  warn?: boolean;
  success?: boolean;
}

export const FormControl: React.SFC<IFormControlProps> =
  ({ children, className, style, error, warn, success, ...domProps }: IFormControlProps) => {
    const classes = {
      'br-form-control': true,
      'br-form-control--error': error,
      'br-form-control--warn': warn,
      'br-form-control--success': success,
    };

    return (
      <div
        {...domProps as IDOMProps}
        className={classNames(classes, className)}
        style={style}
      >
        {children}
      </div>
    );
  };

FormControl.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  style: PropTypes.object,
  error: PropTypes.bool,
  warn: PropTypes.bool,
  success: PropTypes.bool,
};

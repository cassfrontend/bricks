import PropTypes from 'prop-types';
import React from 'react';
import { getKeys } from '../../utils/utility';

export interface IColProps {
  xs?: number;
  sm?: number;
  md?: number;
  lg?: number;

  xsOffset?: number;
  smOffset?: number;
  mdOffset?: number;
  lgOffset?: number;

  xsPull?: number;
  smPull?: number;
  mdPull?: number;
  lgPull?: number;

  xsPush?: number;
  smPush?: number;
  mdPush?: number;
  lgPush?: number;

  xsHidden?: boolean;
  smHidden?: boolean;
  mdHidden?: boolean;
  lgHidden?: boolean;

  className?: string;
  style?: React.CSSProperties;
  children?: React.ReactNode;
}

const colClasses = {
  xs: 'col-xs',
  sm: 'col-sm',
  md: 'col-md',
  lg: 'col-lg',

  xsOffset: 'col-xs-offset',
  smOffset: 'col-sm-offset',
  mdOffset: 'col-md-offset',
  lgOffset: 'col-lg-offset',

  xsPull: 'col-xs-pull',
  smPull: 'col-sm-pull',
  mdPull: 'col-md-pull',
  lgPull: 'col-lg-pull',

  xsPush: 'col-xs-push',
  smPush: 'col-sm-push',
  mdPush: 'col-md-push',
  lgPush: 'col-md-push',
};

const hiddenMap = {
  xsHidden: 'hidden-xs',
  smHidden: 'hidden-xs',
  mdHidden: 'hidden-xs',
  lgHidden: 'hidden-xs',
};

export const Col: React.SFC<IColProps> = (props: IColProps) => {
  const classNames: string[] = [];
  getKeys(colClasses).forEach((key) => {
    if (props[key]) {
      const col = `${props[key]}`.replace('.', '-');
      classNames.push(`${colClasses[key]}-${col}`);
    }
  });

  getKeys(hiddenMap).forEach((key) => {
    if (props[key]) {
      classNames.push(hiddenMap[key]);
    }
  });

  const propsClassName = props.className || '';
  const classString = `${classNames.join(' ')} ${propsClassName}`;
  return <div className={classString} style={props.style}>{props.children}</div>;
};

Col.propTypes = {
  xs: PropTypes.number,
  sm: PropTypes.number,
  md: PropTypes.number,
  lg: PropTypes.number,

  xsOffset: PropTypes.number,
  smOffset: PropTypes.number,
  mdOffset: PropTypes.number,
  lgOffset: PropTypes.number,

  xsPull: PropTypes.number,
  smPull: PropTypes.number,
  mdPull: PropTypes.number,
  lgPull: PropTypes.number,

  xsPush: PropTypes.number,
  smPush: PropTypes.number,
  mdPush: PropTypes.number,
  lgPush: PropTypes.number,

  xsHidden: PropTypes.bool,
  smHidden: PropTypes.bool,
  mdHidden: PropTypes.bool,
  lgHidden: PropTypes.bool,

  className: PropTypes.string,
  style: PropTypes.object,
  children: PropTypes.node,
};

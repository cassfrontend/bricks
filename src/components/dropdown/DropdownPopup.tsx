import React from 'react';
import ReactDOM from 'react-dom';
import { config, Transition } from 'react-spring';

import { Collapse } from '../collapse';

export interface IDropdownPopupProps {
  open: boolean;
  dropdownKey?: string;
  className?: string;
  style?: React.CSSProperties;
  children: JSX.Element;
}

export class DropdownPopup extends React.Component<IDropdownPopupProps, {}> {
  public ele: HTMLElement;
  constructor(props: IDropdownPopupProps) {
    super(props);
    this.ele = document.createElement('div');
    this.ele.className = `dropdown-wrapper ${props.className || ''}`;
  }

  public componentDidMount() {
    document.body.appendChild(this.ele);
  }

  public componentWillUnmount() {
    document.body.removeChild(this.ele);
  }

  public render() {
    const {style, open, children} = this.props;
    return ReactDOM.createPortal(
      <div className="br-dropdown__popup"
        style={{ position: 'absolute', overflow: 'hidden', ...this.props.style }}
      >
        <Collapse active={open}>{this.props.children}</Collapse>
        {/* <Transition
          items={+this.props.open}
          key={this.props.dropdownKey}
          from={{ opacity: 0, height: 0, transform: 'scale(1)' }}
          enter={[{ opacity: 1, height: 'auto' }, { transform: 'scale(1.5)' }]}
          leave={[{ transform: 'scale(1)', opacity: 0.5 }, { opacity: 0 }, { height: 0 }]}
          config={{ ...config.default }}
        >
          {
            (isOpen: any) => isOpen && ((props: React.CSSProperties) =>
            <div style={{
              ...props,
            }}>
              {this.props.children}
            </div>)
          }
        </Transition> */}
      </div >,
      this.ele,
    );
  }
}

import classNames from 'classnames';
import React from 'react';
import { Button } from '../button';
import { Row } from '../row';
import { Modal } from '../modal';

export interface IConfirmProps {
  onClick?: React.MouseEventHandler;
  children?: React.ReactNode;
  showShade?: boolean;
  topTitle?: string;
  tipIcon?: JSX.Element;
  onClose?: () => void;
  onOk?: () => void;
  onCancel?: () => void;
  className?: string;
  style?: React.CSSProperties;
  confirmText?: string;
  cancelText?: string;
}

export interface IConfirmHeaderProps {
  children?: React.ReactNode;
}

export interface IConfirmContentProps {
  children?: React.ReactNode;
}

const Header = (props: IConfirmHeaderProps) => {
  return (
    <Row className="br-confirm__title" >{props.children}</Row>
  );
};

const Content = (props: IConfirmContentProps) => {
  return (
    <div className="br-confirm__content" >
      {props.children}
    </div>
  );
};

export class Confirm extends React.Component<IConfirmProps, {}> {
  public static Header = Header;
  public static Content = Content;
  public ele: Element;
  constructor(props: IConfirmProps) {
    super(props);
    this.handleCloseClick = this.handleCloseClick.bind(this);
    this.handleOkClick = this.handleOkClick.bind(this);
    this.handleCancelClick = this.handleCancelClick.bind(this);
    this.ele = document.createElement('div');
  }

  public componentDidMount() {
    document.body.appendChild(this.ele);
  }

  public componentWillUnmount() {
    document.body.removeChild(this.ele);
  }

  public handleCloseClick() {
    if (this.props.onClose) {
      this.props.onClose();
    }
  }

  public handleOkClick() {
    if (this.props.onOk) {
      this.props.onOk();
    }
  }

  public handleCancelClick() {
    if (this.props.onCancel) {
      this.props.onCancel();
    }
  }

  public render() {
    let informationStyle = {};

    if (this.props.tipIcon) {
      informationStyle = {
        paddingLeft: '62px',
      };
    }
    return (
      <Modal
        className={classNames('br-confirm', this.props.className)}
        style={this.props.style}
        showBackdrop={this.props.showShade} visible={true}
        onBackdropClick={this.props.onClose}>
        <Modal.Header onClose={this.handleCloseClick}>
          <span className="br-confirm__title-bar" >{this.props.topTitle}</span>
        </Modal.Header>
        <Modal.Content>
          <div className="br-confirm__body" >
            {this.props.tipIcon ? <div className="br-confirm__tip-icon" >{this.props.tipIcon}</div> : null}
            {this.props.tipIcon ? <div style={informationStyle} >{this.props.children}</div> : this.props.children}
          </div>
        </Modal.Content>
        <Modal.Footer>
          <div className="br-confirm__footer" >
            <Button className="br-confirm__ok-button" onClick={this.handleOkClick} >
              {this.props.confirmText || '确定'}
            </Button>
            <Button outline onClick={this.handleCancelClick} >
              {this.props.cancelText || '取消'}
            </Button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

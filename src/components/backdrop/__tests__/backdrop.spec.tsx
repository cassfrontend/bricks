import { shallow } from 'enzyme';
import React from 'react';
import { Backdrop } from '../Backdrop';

describe('Backdrop 测试', () => {
  test('Backdrop', () => {
    const backdrop = shallow(<Backdrop visible={true} />);
    expect(backdrop.prop('className')).toEqual('br-backdrop ');
  });

  test('模拟点击', () => {
    const onBackdropClick = jest.fn();
    const backdrop = shallow(<Backdrop visible={true} onBackdropClick={onBackdropClick}/>);
    backdrop.simulate('click');
    // backdrop.find('div.br-backdrop').simulate('click');
    expect(onBackdropClick.mock.calls.length).toBe(1);
    expect(backdrop).toMatchSnapshot();
  });
});

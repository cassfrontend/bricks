import PropTypes from 'prop-types';
import React from 'react';

interface INavbarProps {
  iconClassName?: string;
  description?: string;
  children?: React.ReactNode;
}

interface IItemProps {
  children?: React.ReactNode;
  active?: boolean;
}

const Item: React.SFC<IItemProps> = (props: IItemProps) => {
  return <li className={props.active ? 'active' : ''}>{props.children}</li>;
};

Item.propTypes = {
  children: PropTypes.node,
  active: PropTypes.bool,
};

export default class Navbar extends React.Component<INavbarProps, {}> {
  public static propTypes = {
    iconClassName: PropTypes.string,
    description: PropTypes.string,
    children: PropTypes.node,
  };
  public static Item = Item;
  public render() {
    return (
      <ol className="navbar">
        <i className={`icon ${this.props.iconClassName || 'icon-car'}`} />
        <span className="brand">{this.props.description || ''}</span>
        {this.props.children}
      </ol>
    );
  }
}

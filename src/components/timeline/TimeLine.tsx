import classNames from 'classnames';
import React from 'react';

import { IDOMProps } from '../../utils/props';

export interface ITimelineItemProps extends IDOMProps {
  children: React.ReactNode;
  className?: string;
  style?: React.CSSProperties;
  active?: boolean;
}

export interface ITimelineProps {
  children: React.ReactNode;
  style?: React.CSSProperties;
  className?: string;
}

const Item = (props: ITimelineItemProps) => {
  const { children, className, style, active, ...domProps } = props;
  return (
    <li
      {...domProps as IDOMProps}
      className={classNames('br-time-line__item', className, { 'br-time-line__item--active': active })}
      style={style}
    >
      {children}
    </li>
  );
};

export class Timeline extends React.PureComponent<ITimelineProps, {}>  {
  public static Item = Item;
  public render() {
    return (
      <ul className={classNames('br-time-line', this.props.className)} style={this.props.style}>
        {this.props.children}
      </ul>
    );
  }
}

import * as React from 'react';

import { Button } from '../Button';

import { shallow } from 'enzyme';

describe('Button test', () => { //  测试块描述
  test('Button text', () => { //  单个测试
    const button = shallow(<Button>按钮</Button>);
    expect(button.text()).toEqual('按钮');
    expect(button.hasClass('button'));
  });

  test('Button snapshot', () => {
    const wrapper = shallow(<Button className="btn" onClick={() => null} onMouseDown={() => null}>按钮2</Button>);
    expect(wrapper.prop('className')).toEqual('button btn');
    expect(wrapper).toMatchSnapshot();
  });
});

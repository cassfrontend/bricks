import classNames from 'classnames';
import PropTypes, { array } from 'prop-types';
import React from 'react';

import { IDOMProps } from '../../utils/props';

export interface ICarouselItemProps extends IDOMProps {
  className?: string;
  style?: React.CSSProperties;
  children?: React.ReactChild;
}

export const CarouselItem: React.SFC<ICarouselItemProps> = (props: ICarouselItemProps) => {
  const { className, style, children, ...domProps } = props;
  return (
    <li
      {...domProps as IDOMProps}
      style={style}
      className={classNames('br-carousel__item', className)}
    >
      {children}
    </li>
  );
};

CarouselItem.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
};

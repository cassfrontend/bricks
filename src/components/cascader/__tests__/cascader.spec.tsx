import * as React from 'react';
import { shallow, mount } from 'enzyme';

import { Cascader } from '../Cascader';

describe('Cascader 单元测试', () => {
  test('Cascader 测试', () => {
    const cascader = shallow(<Cascader options={options} />);
    expect(cascader.prop('className')).toBe('br-cascader ');
  });

  test('Cascader 默认值测试', async () => {
    let cascader = await mount(<Cascader options={options} defaultValue={defaultValue} />);
    afterEach(() => {
      expect(cascader.find('.br-cascader__value').text()).toEqual(defaultValue.map((v) => v.name).join(' - '));
    });
  });

  test('Cascader placeholder测试', () => {
    const placeholder = '请选择居住地址';
    const cascader = mount(<Cascader options={options} placeholder={placeholder} />);
    expect(cascader.find('.br-cascader__value').text()).toEqual(placeholder);
  });

  test('Cascader tabTitle测试', () => {
    const cascader = mount(<Cascader options={options} tabTitle={tabTitle} />);
    expect(cascader.find('.br-cascader__tabs').text()).toEqual(tabTitle.join(''));
  });
});

const options = [
  {
    name: '广东',
    id: 'guangdong',
    children: [
      {
        name: '深圳',
        id: 'shenzhen',
        children: [
          {
            name: '龙岗区',
            id: 'longgangqu',
            children: [
              {
                name: '坂田',
                id: 'bantian',
              },
              {
                name: '五和',
                id: 'wuhe',
              },
            ],
          },
          {
            name: '南山区',
            id: 'nanshanqu',
            children: [
              {
                name: '下沙',
                id: 'xiasha',
              },
              {
                name: '深南',
                id: 'shennan',
              },
            ],
          },
        ],
      },
      {
        name: '广州',
        id: 'guangzhou',
        children: [
          {
            name: '白云区',
            id: 'baiyunqu',
          },
        ],
      },
    ],
  },
  {
    name: '浙江',
    id: 'Zhejiang',
    children: [
      {
        name: '杭州',
        id: 'Hangzhou',
        children: [
          {
            name: '西湖',
            id: 'West Lake',
          },
        ],
      },
    ],
  },
  {
    name: '江苏',
    id: 'Jiangsu',
    children: [
      {
        name: '南京',
        id: 'Nanjing',
        children: [
          {
            name: '中华门',
            id: 'Zhong Hua Men',
          },
        ],
      },
    ],
  },
  {
    name: '北京',
    id: 'beijin',
  },
];

const defaultValue = [
  { id: 'guangdong', name: '广东' },
  { id: 'shenzhen', name: '深圳' },
  { id: 'longgangqu', name: '龙岗区' },
  { id: 'bantian', name: '坂田' },
];

const tabTitle = ['省份', '城市', '县区'];

import { shallow } from 'enzyme';
import moment from 'moment';
import Picker from 'rc-calendar/lib/Picker';
import React from 'react';
import { DatePicker } from '../DatePicker';

describe('DatePicker', () => {
  test('simple', () => {
    const wrapper = shallow(<DatePicker value={moment('2019-01-01')} />);
    expect(wrapper.find(Picker).render().find('input')[0].attribs.value).toEqual('2019-01-01');
  });
  test('showTime', () => {
    const wrapper = shallow(<DatePicker value={moment('2019-01-01 12:00:00')} showTime/>);
    expect(wrapper.find(Picker).render().find('input')[0].attribs.value).toEqual('2019-01-01 12:00:00');
  });
  test('showSecond', () => {
    const wrapper = shallow(<DatePicker value={moment('2019-01-01 12:00:00')} showTime showSecond={false}/>);
    expect(wrapper.find(Picker).render().find('input')[0].attribs.value).toEqual('2019-01-01 12:00');
  });
});

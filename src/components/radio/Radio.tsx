import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import { IDOMProps } from '../../utils/props';

export interface IRadioProps extends IDOMProps {
  name?: string;
  checked?: boolean; // 从外部可以控制 是否选中
  value?: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  disabled?: boolean;
  className?: string;
  style?: React.CSSProperties;
}

export const Radio: React.SFC<IRadioProps> = (props: IRadioProps) => {
  const { name, checked, disabled, style, className, ...domProps } = props;
  return (
    <label
      {...domProps as IDOMProps}
      className={classNames('radio-wrapper radio-box', className)}
      style={style}
    >
      <span className={classNames('radio-box', {
        'radio-checked': checked,
        'radio-disabled': disabled,
      })}>
        <input {...props} type="radio" className="radio-box-input" />
        <span className="radio-box-inner"></span>
      </span>
      <span className="radio-name">{name}</span>
    </label>
  );
};

Radio.propTypes = {
  name: PropTypes.string,
  checked: PropTypes.bool, // 从外部可以控制 是否选中
  value: PropTypes.string,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  className: PropTypes.string,
  style: PropTypes.object,
};

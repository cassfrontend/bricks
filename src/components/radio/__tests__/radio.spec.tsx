import React from 'react';
import { Radio } from '../Radio';
import { shallow } from 'enzyme';

describe('Radio checked属性测试', () => {
  test('radio 设置checked', () => {
    const radio = shallow(<Radio checked />);
    expect(radio.find('input').prop('checked')).toEqual(true);
    expect(radio.find('span.radio-box').hasClass('radio-checked'));
  });

  test('radio 设置checked false', () => {
    const radio = shallow(<Radio checked={false} />);
    expect(radio.find('input').prop('checked')).toEqual(false);
    expect(radio.find('span.radio-box').prop('className')).toEqual('radio-box');
  });

  test('radio 没有设置checked', () => {
    const radio = shallow(<Radio />);
    expect(radio.find('input').prop('checked')).toEqual(undefined);
  });
});

describe('Radio type value name属性测试', () => {
  test('radio 设置type value name', () => {
    const radio = shallow(<Radio name="form-radio" value="123" />);
    expect(radio.find('input').prop('type')).toEqual('radio');
    expect(radio.find('input').prop('value')).toEqual('123');
    expect(radio.find('input').prop('name')).toEqual('form-radio');
  });
});

it('Radio 模拟点击', () => {
  const onRadioChange1 = jest.fn();
  const onRadioChange2 = jest.fn();
  const radio = shallow(<Radio className="form-radio" name="form-radio" value="123" onChange={onRadioChange1} />);

  radio.find('input').simulate('change');
  expect(onRadioChange1).toHaveBeenCalled();
  expect(onRadioChange1.mock.calls.length).toBe(1);
  expect(radio).toMatchSnapshot();

  radio.update();
  radio.setProps({
    disabled: true,
    onChange: onRadioChange2,
  });
  expect(radio.find('input').prop('disabled')).toEqual(true);
  radio.find('input').simulate('click');
  expect(onRadioChange2.mock.calls.length).toBe(0);
  expect(radio).toMatchSnapshot();
});

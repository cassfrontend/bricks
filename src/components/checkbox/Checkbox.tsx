import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import { IControledDOMProps } from '../../utils/props';

export interface ICheckboxProps extends IControledDOMProps {
  name?: string;
  title?: string;
  defaultChecked?: boolean;
  checked?: boolean;
  className?: string;
  style?: React.CSSProperties;
  onToggle?: (checked: boolean) => void;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  disabled?: boolean;
  value?: string;
}

export interface ICheckboxState {
  checked?: boolean;
}
export class Checkbox extends React.Component<ICheckboxProps, ICheckboxState> {
  public static propTypes = {
    name: PropTypes.string,
    title: PropTypes.string,
    defaultChecked: PropTypes.bool,
    checked: PropTypes.bool,
    className: PropTypes.string,
    onToggle: PropTypes.func,
  };
  // 如果传入了外部props，更新到state
  public static getDerivedStateFromProps(nextProps: ICheckboxProps, prevState: ICheckboxState) {
    if (nextProps.checked !== undefined) {
      return {
        checked: nextProps.checked,
      };
    }
    return null;
  }

  constructor(props: ICheckboxProps) {
    super(props);
    this.state = {
      checked: props.defaultChecked || props.checked || false,
    };
    this.handleToggle = this.handleToggle.bind(this);
  }

  public handleToggle(event: React.ChangeEvent<HTMLInputElement>) {
    const checked = event.target.checked;
    const { onToggle, onChange } = this.props;

    if (onToggle || onChange) {
      if (onToggle) {
        onToggle(checked);
      } else if (onChange) {
        onChange(event as any);
      }
    }
    this.setState({
      checked,
    });
  }

  public render() {
    const { className = '', style, name, title = '', value, disabled, ...domProps } = this.props;
    const { checked } = this.state;
    return (
      <label
        {...domProps as IControledDOMProps}
        className={
          classNames('br-checkbox', className,
            { 'br-checkbox--disabled': disabled },
            { 'br-checkbox--checked': checked },
          )
        }
        style={style}
      >
        <span className="br-checkbox__icon" />
        <input
          type="checkbox"
          name={name}
          checked={checked}
          className="hidden"
          value={value}
          onChange={this.handleToggle.bind(this)}
          disabled={disabled} />
        <span className="br-checkbox__title">{title}</span>
      </label>
    );
  }
}

import moment, { Moment } from 'moment';

export type Moment = Moment;

export interface IDateRange {
  startTime: Moment;
  endTime: Moment;
}

export interface IDateRangePanelItem {
  name: string;
  value: number;
  year?: number;
}

export interface IDateRangeValue {
  name?: string;
  type: string;
  value: number;
  year: number;
}

export function createDateRangeItem(type: string, year: number, startYear?: number, endYear?: number) {
  let items: IDateRangePanelItem[] = [];

  switch (type) {
    case 'month':
      items = [
        {
          name: '全年',
          value: -1,
        },
        {
          name: '1月',
          value: 1,
        },
        {
          name: '2月',
          value: 2,
        },
        {
          name: '3月',
          value: 3,
        },
        {
          name: '4月',
          value: 4,
        },
        {
          name: '5月',
          value: 5,
        },
        {
          name: '6月',
          value: 6,
        },
        {
          name: '7月',
          value: 7,
        },
        {
          name: '8月',
          value: 8,
        },
        {
          name: '9月',
          value: 9,
        },
        {
          name: '10月',
          value: 10,
        },
        {
          name: '11月',
          value: 11,
        },
        {
          name: '12月',
          value: 12,
        },
      ];
      break;
    case 'quarter':
      items = [
        {
          name: '全年',
          value: -1,
        },
        {
          name: '第 1 季度',
          value: 1,
        },
        {
          name: '第 2 季度',
          value: 2,
        },
        {
          name: '第 3 季度',
          value: 3,
        },
        {
          name: '第 4 季度',
          value: 4,
        },
      ];
      break;
    case 'half':
      items = [
        {
          name: '全年',
          value: -1,
        },
        {
          name: '上半年',
          value: 1,
        },
        {
          name: '下半年',
          value: 2,
        },
      ];
      break;
    case 'year':
      let selectedIndex = 0;
      let start = (startYear ? (endYear  ? (startYear < endYear ? startYear : endYear) : startYear) : 0) || moment().year() - 10;
      let end = (endYear ? (startYear  ? (endYear < startYear ? startYear : endYear) : endYear) : 0) || moment().year();

      for (let i = start; i <= end; i++ , selectedIndex++) {
        items.push({
          name: `${i + '年'}`,
          value: i,
        });
      }
      break;
    case 'week':
      let momentTime = moment().year(year);
      let weeks = momentTime.isoWeeksInYear();
      let week = 0;

      if (momentTime.week(1).startOf('isoWeek').format('YYYY') !== year.toString()) {
        week = 2;
        items.push({
          name: '第1周',
          value: 2,
        });
      }

      for (; week < weeks; week++) {
        items.push({
          name: `第${week + 1}周`,
          value: week + 1,
        });
      }

      if (moment().year(year + 1).week(1).startOf('isoWeek').format('YYYY') === year.toString()) {
        items.push({
          name: `第${items.length + 1}周`,
          value: items.length + 1,
        });
      }

      break;
  }

  return items;
}

export function getRange(date: IDateRangeValue, flag?: boolean): IDateRange {
  if (date.value === -1) {
    return {
      startTime: moment().year(date.year).startOf('year'),
      endTime: moment().year(date.year).endOf('year'),
    };
  }

  let startTime;
  let endTime;

  switch (date.type) {
    case 'month':
      startTime = moment().year(date.year).month(date.value - 1).startOf('month');
      endTime = moment().year(date.year).month(date.value - 1).endOf('month');
      break;
    case 'quarter':
      startTime = moment().year(date.year).quarter(date.value).startOf('quarter');
      endTime = moment().year(date.year).quarter(date.value).endOf('quarter');
      break;
    case 'half':
      let startValue = date.value === 1 ? 1 : 3;
      let endValue = date.value === 1 ? 2 : 4;
      startTime = moment().year(date.year).quarter(startValue).startOf('quarter');
      endTime = moment().year(date.year).quarter(endValue).endOf('quarter');
      break;
    case 'year':
      startTime = moment().year(date.year).startOf('year');
      endTime = moment().year(date.year).endOf('year');
      break;
    default:
      if (flag) {
        startTime = moment().year(date.year).isoWeek(date.value).startOf('isoWeek');
        endTime = moment().year(date.year).isoWeek(date.value).endOf('isoWeek');
      } else {
        if (moment().year(date.year).week(1).startOf('isoWeek').format('YYYY') !== date.year.toString()) {
          startTime = moment().year(date.year).isoWeek(date.value + 1).startOf('isoWeek');
          endTime = moment().year(date.year).isoWeek(date.value + 1).endOf('isoWeek');
        } else {
          startTime = moment().year(date.year).isoWeek(date.value).startOf('isoWeek');
          endTime = moment().year(date.year).isoWeek(date.value).endOf('isoWeek');
        }
      }
  }

  return {
    startTime,
    endTime,
  };
}

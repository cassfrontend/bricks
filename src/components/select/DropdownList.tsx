import classNames from 'classnames';
import PropTypes, { any } from 'prop-types';
import React from 'react';
import ReactDOM from 'react-dom';
import { isScrollBarVisible } from '../../utils/utility';
import { Icon } from '../icon';
import { Input } from '../input';
import { VirtualizedList } from './VirtualizedList';

export type OptionValues = string;

export type Options<TValue = OptionValues> = Array<IOption<TValue>>;

export interface IOption<TValue = OptionValues> {
  /** Text for rendering */
  label?: string;
  /** Value for searching */
  value?: TValue;
  disabled?: boolean;
}

export interface IDropdownListProps {
  emptyTips?: React.ReactNode;
  onFilterChange: (text: string) => void;
  options: Options;
  onInputClick: (e: React.MouseEvent<HTMLInputElement | HTMLDivElement>) => void;
  onItemClick: (option: IOption, index: number) => void;
  value: IOption[];
  type: string;
}

export class DropdownList extends React.Component<IDropdownListProps, {}> {
  public static propTypes = {
    emptyTips: PropTypes.node,
    onFilterChange: PropTypes.func.isRequired,
    onInputClick: PropTypes.func.isRequired,
    onItemClick: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired,
  };

  constructor(props: IDropdownListProps) {
    super(props);
    this.updateMenuListClassName = this.updateMenuListClassName.bind(this);
  }

  public componentDidMount() {
    this.updateMenuListClassName();
  }
  public componentDidUpdate() {
    this.updateMenuListClassName();
  }

  public updateMenuListClassName() {
    const dom = ReactDOM.findDOMNode(this);
    if (dom && typeof dom !== 'string') {
      const list = (dom as HTMLElement).getElementsByClassName('br-select__menu-list')[0];
      if (list) {
        const className = list.className;
        const newClassName = classNames(className, {
          'br-select__menu-list--scroll br-scroll': isScrollBarVisible(list as HTMLElement),
        });
        list.className = newClassName;
      }
    }
  }

  public render() {
    const { onFilterChange, options, onInputClick, onItemClick, value, type } = this.props;
    let ulHeight = 26;
    let searchElement;
    if (type === 'search') {
      searchElement = <div className="br-select__search-box">
        <Input
          small
          onClick={onInputClick}
          autoFocus
          icon={<Icon type="search" size={12} color="#888" />}
          onChange={(e) => onFilterChange((e.target as { value: string }).value)}
        />
      </div>;
    }

    if (options.length) {
      if (options.length >= 10) {
        ulHeight = 240;
      } else {
        ulHeight = options.length * 26;
      }
    }

    return (
      <div className="br-select__menu" onClick={onInputClick}>
        {searchElement}
        <ul className="br-select__menu-list" style={{ height: ulHeight }}>
          {options.length ? <VirtualizedList
           options={options} value={value} onItemClick={onItemClick}></VirtualizedList> : <li
            className="br-select__menu-item br-select__menu-item--disabled">暂无数据</li>}
        </ul>
      </div>
    );
  }
}

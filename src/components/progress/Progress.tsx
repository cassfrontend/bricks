import React from 'react';

import { IDOMProps } from '../../utils/props';

import { CircleProgress } from './CircleProgress';
import { LineProgress } from './LineProgress';

export interface IProgressProps extends IDOMProps {
  /** 进度条类型 line:线性 circle:环形 */
  type?: 'line' | 'circle';
  className?: string;
  style?: React.CSSProperties;
  /** 进度条百分比 */
  percent?: number;
  /** 进度条宽度 */
  strokeWidth?: number;
  /** 进度条颜色 */
  strokeColor?: string;
  /** 进度条轨迹填充色 */
  trailColor?: string;
  /** 进度条边缘形状, round:圆角 square：方角 */
  strokeLinecap?: 'round' | 'square';
  /** 是否显示进度数值 */
  format?: (percent: number) => (React.ReactNode | string);
  /** 获取文案内容函数 */
  showText?: boolean;
  /** 视窗宽度 */
  width?: number;
  /** 视窗高度 */
  height?: number;
  /** 开始角度 */
  startDegree?: number;
}

export const Progress: React.SFC<IProgressProps> = (props: IProgressProps) => {
  const { type } = props;
  if (type === 'circle') {
    return <CircleProgress {...props} />;
  }
  return <LineProgress {...props} />;
};


import React, { ReactChild, ReactElement } from 'react';
import classNames from 'classnames';

import { IControledDOMProps } from '../../utils/props';

import { ITabItemProps } from './Tab';

interface ITabsProps extends IControledDOMProps {
  activeKey?: string;
  onChange: (activeKey: string) => void;
  children?: Array<React.ReactElement<ITabItemProps>>;
  className?: string;
  style?: React.CSSProperties;
  align?: string;
}

export class Tabs extends React.Component<ITabsProps, {}> {

  public render() {
    const { activeKey, onChange, className, style, align, ...domProps } = this.props;
    return (
      <div
        {...domProps as IControledDOMProps}
        className={classNames('br-tabs', className)}
        style={style}
      >
        <div className={classNames('br-tabs__head', `align-${align}`)} >
          <ul>
            {
              React.Children.map<ReactElement<ITabItemProps>>(this.props.children,
                (child: ReactChild, index) => {
                  const ele = child as ReactElement<ITabItemProps>;
                  return (
                    <li className={`item${index}`}>
                      {
                        React.cloneElement(ele, {
                          onChange,
                          active: activeKey === ele.props.value,
                        })
                      }
                    </li>
                  );
                })
            }
          </ul>
        </div>
      </div>
    );
  }
}

/**
 * <Tabs activeKey="tab2">
 *   <Tab value="tab1" active={false}>按钮</Tab>
 * </Tabs>
 */

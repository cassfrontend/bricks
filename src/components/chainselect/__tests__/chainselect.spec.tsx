import * as React from 'react';
import { shallow, mount } from 'enzyme';

import { ChainSelect } from '../ChainSelect';

const options = [
  {
    label: '广东',
    value: 'guangdong',
    children: [
      {
        label: '深圳',
        value: 'shenzhen',
        children: [
          {
            label: '龙岗',
            value: 'longgangqu',
            children: [
              {
                label: '坂田',
                value: 'bantian',
              },
              {
                label: '五和',
                value: 'wuhe',
              },
            ],
          },
          {
            label: '南山',
            value: 'nanshanqu',
            children: [
              {
                label: '下沙',
                value: 'xiasha',
              },
              {
                label: '深南',
                value: 'shennan',
              },
            ],
          },
        ],
      },
      {
        label: '广州',
        value: 'guangzhou',
        children: [
          {
            label: '白云',
            value: 'baiyunqu',
          },
        ],
      },
    ],
  },
  {
    label: '浙江',
    value: 'Zhejiang',
    children: [
      {
        label: '杭州',
        value: 'Hangzhou',
        children: [
          {
            label: '西湖',
            value: 'West Lake',
          },
        ],
      },
    ],
  },
  {
    label: '江苏',
    value: 'Jiangsu',
    children: [
      {
        label: '南京',
        value: 'Nanjing',
        children: [
          {
            label: '中华门',
            value: 'Zhong Hua Men',
          },
        ],
      },
    ],
  },
  {
    label: '北京',
    value: 'beijin',
  },
];

const titles = [
  { value: '', label: '省' },
  { value: '', label: '市' },
  { value: '', label: '区' },
  { value: '', label: '街道' },
];

const defaultValue = [
  { value: 'guangdong', label: '广东' },
  { value: 'shenzhen', label: '深圳' },
  { value: 'longgangqu', label: '龙岗' },
  { value: 'bantian', label: '坂田' },
];

describe('ChainSelect 单元测试', () => {
  const randomIndex = Math.floor(Math.random() * titles.length);
  test('ChainSelect className测试', () => {
    const className = 'area-chainselect';
    const chainSelect = shallow(<ChainSelect options={options} className={className} />);
    expect(chainSelect.hasClass('br-chainselect')).toBeTruthy();
    expect(chainSelect.hasClass(className)).toBeTruthy();
  });

  test('ChainSelect style样式测试', () => {
    const style = {
      color: 'red',
      fontSize: 14,
      backgroundColor: 'red',
    };
    const chainSelect = shallow(<ChainSelect
      style={style}
      options={options}
    />);
    expect(chainSelect.find('.br-chainselect').props().style).toBe(style);
  });

  test('ChainSelect 的级联数测试', () => {
    const chainSelect = mount(<ChainSelect
      options={options}
      titles={titles}
    />);
    const selectList = chainSelect.find('Select');
    expect(selectList.at(randomIndex).hasClass(`br-chainselect__depth-${titles.length}`)).toBeTruthy();
    expect(chainSelect.find('.br-select').length).toBe(titles.length);
  });

  test('ChainSelect 默认值测试', () => {
    const chainSelect = mount(<ChainSelect
      options={options}
      titles={titles}
      defaultValue={defaultValue}
    />);
    const contentList = chainSelect.find('.br-select__value-content');
    defaultValue.forEach((v, i) => {
      expect(contentList.at(i).text()).toBe(v.label);
    });
  });

  test('ChainSelect 受控特性测试', () => {
    const value = [
      { value: 'guangdong', label: '广东省' },
      { value: 'shenzhen', label: '深圳市' },
      { value: 'longgangqu', label: '龙岗区' },
    ];
    const chainSelect = mount(<ChainSelect
      options={options}
      titles={titles}
      value={value}
    />);
    const contentList = chainSelect.find('.br-select__value-content');
    value.forEach((v, i) => {
      expect(contentList.at(i).text()).toBe(v.label);
    });
  });

  test('ChainSelect 有可筛选选择框测试', () => {
    const chainSelect = mount(<ChainSelect
      options={options}
      searchable
    />);
    const selectList = chainSelect.find('Select');
    selectList.forEach((v, i) => {
      expect(v.props().type).toEqual('search');
    });
  });

  test('ChainSelect 有可筛选选择框测试', () => {
    const chainSelect = mount(<ChainSelect
      options={options}
      searchable
    />);
    const selectList = chainSelect.find('Select');
    selectList.forEach((v, i) => {
      expect(v.props().type).toEqual('search');
    });
  });

});

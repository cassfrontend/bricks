import classNames from 'classnames';
import React from 'react';

import { IControledDOMProps } from '../../utils/props';

export interface IMenuItemProps extends IControledDOMProps {
  className?: string;
  style?: React.CSSProperties;
  small?: boolean;
  large?: boolean;
  name?: string;
  active?: boolean;
  disabled?: boolean;
  onClick?: (name?: string) => void;
}

export class MenuItem extends React.Component<IMenuItemProps, {}> {
  public onClick() {
    if (!this.props.disabled && this.props.onClick) {
      this.props.onClick(this.props.name);
    }
  }

  public render() {
    const {
      className,
      style,
      children,
      small,
      large,
      active,
      disabled,
      ...domProps
    } = this.props;
    return (
      <li
        {...domProps as IControledDOMProps}
        className={classNames('br-menu__item', className, {
          'br-menu__item--small': small,
          'br-menu__item--large': large,
          'br-menu__item--active': active,
          'br-menu__item--disabled': disabled,
        })}
        style={style}
        onClick={this.onClick.bind(this)}
      >
        {children}
      </li>
    );
  }
}

import classNames from 'classnames';
import React from 'react';
import { Spring, animated } from 'react-spring';
import { IDOMProps } from '../../utils/props';
import { Icon } from '../icon';

export interface ISubMenuProps extends IDOMProps {
  className?: string;
  style?: React.CSSProperties;
  title: string;
  icon?: React.ReactDOM;
  triggerEvent?: string;
  outter?: string;
}

export interface ISubMenuState {
  active: boolean;
}

export class SubMenu extends React.Component<ISubMenuProps, ISubMenuState> {
  public static defaultProps = {
    triggerEvent: 'hover',
  };

  constructor(props: ISubMenuProps) {
    super(props);

    this.state = {
      active: false,
    };

    this.onClick = this.onClick.bind(this);
    this.onTitleClick = this.onTitleClick.bind(this);
    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
  }

  public onClick() {
    this.setState({
      active: !this.state.active,
    });
  }

  public onTitleClick(e: React.MouseEvent) {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      active: !this.state.active,
    });
  }

  public onMouseEnter() {
    this.setState({
      active: true,
    });
  }

  public onMouseLeave() {
    this.setState({
      active: false,
    });
  }

  public render() {
    const { triggerEvent, className, children, style, title, icon, outter, ...domProps } = this.props;
    const { active } = this.state;
    return (
      <li
        {...domProps as IDOMProps}
        className={classNames('br-menu__item br-submenu', className)}
        style={style}
        onMouseEnter={triggerEvent === 'hover' ? this.onMouseEnter : () => null}
        onMouseLeave={triggerEvent === 'hover' ? this.onMouseLeave : () => null}
        onClick={triggerEvent === 'click' ? this.onClick : () => null}
      >
        <div className="br-submenu__warpper" onClick={triggerEvent === 'click' ? this.onTitleClick : () => null}>
          <span
            className="br-submenu__title text-overflow"
            title={title}
          >
            {icon}
            {title}
          </span>
          <Icon
            className={classNames(
              'pull-right br-submenu__icon',
              { 'br-submenu__icon--active': active },
            )}
            type={outter ? 'right' : 'down'}
            size={14}
          />
        </div>
        <Spring
          native
          force
          config={{ tension: 2000, friction: 100, precision: 1 }}
          from={{ height: active ? 0 : 'auto' }}
          to={{ height: active ? 'auto' : 0 }}>
          {(props) => (
            <animated.div
              className={classNames('br-submenu__container', {
                'br-submenu__container-active': active,
                'br-submenu__container--outter': outter,
              })}
              style={props}
            >
              <ul className="br-submenu__list">
                {children}
              </ul>
            </animated.div>
          )}
        </Spring>
      </li>
    );
  }
}

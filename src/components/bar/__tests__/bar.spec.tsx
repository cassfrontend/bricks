import { shallow } from 'enzyme';
import React from 'react';
import { Bar } from '../Bar';

describe('bar 属性测试', () => {
  test('bar 显示是否正确', () => {
    const bar = shallow(<Bar />);
    expect(bar.prop('className')).toEqual('br-bar');
  });
  // test('是否有返回按钮', () => {
  //   const bar = shallow(<Bar showGoBackButton={true} ></Bar>);
  //   expect(bar.find('div.br-bar__back').prop('className')).toEqual('br-bar__back');
  // });
  // test('返回按钮是否生效', () => {
  //   const onBarClick = jest.fn();
  //   const bar = shallow(<Bar showGoBackButton={true} onGoBack={onBarClick}>横幅</Bar>);
  //   bar.find('div.br-bar__back').simulate('click');
  //   expect(onBarClick.mock.calls.length).toBe(1);
  //   expect(bar).toMatchSnapshot();
  // });
});

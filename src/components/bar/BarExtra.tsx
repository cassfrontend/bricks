import React from 'react';

import { IDOMProps } from '../../utils/props';

export interface IBarExtra extends IDOMProps {
  children: React.ReactNode;
}

export class BarExtra extends React.Component<IBarExtra, {}> {
  public render() {
    const { children, ...domProps } = this.props;
    return (
      <div
        {...domProps as IDOMProps}
        className="br-bar__extra-tool__right"
      >
        {children}
      </div>
    );
  }
}

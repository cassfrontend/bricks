import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import { IDOMProps } from '../../utils/props';

export interface IPanelProps extends IDOMProps {
  children: React.ReactNode;
  header?: React.ReactNode;
  className?: string;
  style?: React.CSSProperties;
}

export const Panel: React.SFC<IPanelProps> = (props: IPanelProps) => {
  const { header, children, className, style, ...domProps } = props;
  return (
    <div
      {...domProps as IDOMProps}
      className={classNames('br-panel br-panel-default', className)}
      style={style}
    >
      {props.header ? <div className="br-panel__header br-panel-default__header">
        {header}
      </div> : null}
      <div className="br-panel__body">
        {children}
      </div>
    </div>
  );
};

Panel.propTypes = {
  children: PropTypes.node,
  header: PropTypes.node,
  style: PropTypes.object,
  className: PropTypes.string,
};

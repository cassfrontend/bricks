module.exports = {
  "presets": [
    ["@babel/preset-env", {
      "modules": false,
      "loose": true,
      "targets": {
        "browsers": ["last 4 versions", "not ie <= 8"]
      },
    }],
    "@babel/preset-typescript",
    "@babel/preset-react"
  ],
  "plugins": [
    ["@babel/plugin-proposal-class-properties", { "loose": true }],
    ["@babel/plugin-proposal-object-rest-spread", { "loose": true }],
    "@babel/plugin-transform-object-assign",
    ["@babel/plugin-transform-runtime", { "useESModules": true }],
    ["transform-react-remove-prop-types",  { "mode": "unsafe-wrap" }]
  ],
  ignore: ['*/__tests__/*']
}

